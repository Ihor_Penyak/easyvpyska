﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EasyVpyska.Repositories.Abstract;
using System.Data;
using System.Data.SqlClient;
using EasyVpyska.Entities;

namespace EasyVpyska.Repositories.Sql
{
    [System.ComponentModel.DataObject]
    public class AdRepository : IAdRepository
    {
        #region Queries

        private const string DELETE_AD = "DELETE FROM tblAd WHERE Id = @Id";

        private const string DELETE_AD_COMMENT_BY_USER_ID = "DELETE FROM tblAdComment WHERE UserId = @UserId";

        private const string DELETE_AD_COMMENT_BY_ID = "DELETE FROM tblAdComment WHERE Id = @Id";

        private const string SELECT_AD = "SELECT Id, IsRequest, IsEnabled, Title, AuthorId, Description, Begin_date, End_date, Country, Town, Address, IsActive  FROM tblAd WHERE Id = @id";

        private const string SELECT_ALL_ADS = "SELECT Id, IsRequest, IsEnabled, Title, AuthorId, Description, Begin_date, End_date, Country, Town, Address, IsActive FROM tblAd";

        private const string SELECT_ALL_ADS_BY_TYPE = "SELECT Id, IsRequest, IsEnabled, Title, AuthorId, Description, Begin_date, End_date, Country, Town, Address, IsActive  FROM tblAd WHERE IsRequest = @IsRequest";

        private const string SELECT_ALL_ADS_BY_USER = "SELECT Id, IsRequest, IsEnabled, Title, AuthorId, Description, Begin_date, End_date, Country, Town, Address, IsActive  FROM tblAd WHERE AuthorId = @AuthorId";

        private const string COUNT_ALL_ADS_BY_TYPE = "SELECT COUNT(*) AS NumberOfRequestAds FROM tblAd WHERE IsRequest = @IsRequest";

        private const string SELECT_VIEW = "SELECT Id FROM tblAdViewers WHERE UserId = @userId AND AdId=@adId";

        private const string SELECT_SUBSCRIBERS = "SELECT Id, AdId, UserId, [Date] FROM tblAdSubscribers WHERE AdId=@adId";

        private const string INSERT_NEW_AD = @"INSERT INTO [dbo].[tblAd]
        (IsRequest, IsEnabled, Title, AuthorId, Description, Begin_date, End_date, Country, Town, Address, IsActive )
        OUTPUT INSERTED.ID
        VALUES	(@IsRequest, @IsEnabled, @Title, @AuthorId, @Description, @Begin_date, @End_date, @Country, @Town, @Address, @Active);";

        private const string INSERT_NEW_VIEW = @"INSERT INTO [dbo].[tblAdViewers]
        (AdId, UserId)
        OUTPUT INSERTED.ID
        VALUES	(@AdId, @UserId);";

        private const string INSERT_NEW_SUBSCRIBERS = @"INSERT INTO [dbo].[tblAdSubscribers]
        (AdId, UserId, [Date])
        OUTPUT INSERTED.ID
        VALUES	(@AdId, @UserId, GETDATE());";

        private const string INSERT_AD_COMMENT = @"INSERT INTO [dbo].[tblAdComment] ([AdId], [IsEnabled], [UserId],[Time],[Comment]) OUTPUT INSERTED.ID VALUES (@AdId, @IsEnabled, @UserId, @Time, @Comment)";

        private const string SELECT_ALL_COMMENTS = "SELECT Id, IsEnabled, AdId, UserId, [Time], [Comment] FROM tblAdComment";

        #endregion

        #region Fields

        private readonly string _connectionString;

        #endregion

        #region Constructor

        public AdRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        #endregion

        #region IAdRepository

        public AdAuthor GetAdAuthorById(int id)
        {
            AdAuthor author = null;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspGetAdAuthorById", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@id", id);

                    con.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            author = new AdAuthor();
                            author.Id = (int)reader["Id"];
                            author.FirstName = (string)reader["Firstname"];
                            author.SurName = reader.IsDBNull(reader.GetOrdinal("Surname")) ? string.Empty : (string)reader["Surname"];
                            author.Email = (string)reader["Email"];
                            author.Town = reader.IsDBNull(reader.GetOrdinal("Town")) ? string.Empty : (string)reader["Town"];
                            author.Status = (bool)reader["Status"];
                            DateTime birthDate = (DateTime)reader["DateOfBirth"];
                            DateTime todayDate = DateTime.Today;
                            TimeSpan timeSpan = (TimeSpan)(todayDate - birthDate);
                            author.Age = (int)(timeSpan.TotalDays / 365.0);
                        }
                    }
                }
            }
            return author;
        }

        public Ad GetAd(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspGetAdById", con))
                {
                    con.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@id", Id);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows && reader.Read())
                        {
                            Ad searchedAd = new Ad();

                            searchedAd.Id = (int)reader["Id"];
                            searchedAd.IsRequest = (bool)reader["IsRequest"];
                            searchedAd.IsEnabled = (bool)reader["IsEnabled"];
                            searchedAd.IsActive = (bool)reader["IsActive"];
                            searchedAd.Title = (string)reader["Title"];
                            searchedAd.Description = (string)reader["Description"];
                            searchedAd.BeginTime = (DateTime)reader["Begin_date"];
                            searchedAd.EndTime = (DateTime)reader["End_date"];
                            searchedAd.Country = (string)reader["Country"];
                            searchedAd.Town = (string)reader["Town"];
                            searchedAd.Address = reader.IsDBNull(reader.GetOrdinal("Address")) ? string.Empty : (string)reader["Address"];
                            searchedAd.Comments = this.GetAdComments(Id);
                            searchedAd.Views = this.CountAdViewing(Id);
                            searchedAd.Subscribers = this.GetAdSubscribers(Id);

                            AdAuthor tempAuthor = new AdAuthor();
                            tempAuthor.Id = (int)reader["AuthorId"];
                            tempAuthor.FirstName = (string)reader["Firstname"];
                            tempAuthor.SurName = reader.IsDBNull(reader.GetOrdinal("Surname")) ? string.Empty : (string)reader["Surname"];
                            tempAuthor.Email = (string)reader["Email"];
                            tempAuthor.Town = reader.IsDBNull(reader.GetOrdinal("AuthorTown")) ? "undefined" : (string)reader["AuthorTown"];
                            tempAuthor.Status = (bool)reader["Status"];
                            DateTime birthDate = (DateTime)reader["DateOfBirth"];
                            DateTime todayDate = DateTime.Today;
                            TimeSpan timeSpan = (TimeSpan)(todayDate - birthDate);
                            tempAuthor.Age = (int)(timeSpan.TotalDays / 365.0);
                            searchedAd.Author = tempAuthor;

                            return searchedAd;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public List<Ad> GetAllAdsByType(bool isRequest)
        {
            List<Ad> result = new List<Ad>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                string query = SELECT_ALL_ADS_BY_TYPE;

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    con.Open();
                    command.Parameters.AddWithValue("@IsRequest", isRequest);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Ad tempAd = new Ad();
                            tempAd.Id = (int)reader["Id"];
                            tempAd.IsRequest = (bool)reader["IsRequest"];
                            tempAd.IsEnabled = (bool)reader["IsEnabled"];
                            tempAd.IsActive = (bool)reader["IsActive"];
                            tempAd.Title = (string)reader["Title"];
                            tempAd.Description = (string)reader["Description"];
                            tempAd.BeginTime = (DateTime)reader["Begin_date"];
                            tempAd.EndTime = (DateTime)reader["End_date"];
                            tempAd.Country = (string)reader["Country"];
                            tempAd.Town = (string)reader["Town"];

                            int AuthorId = (int)reader["AuthorId"];
                            tempAd.Author = this.GetAdAuthorById(tempAd.Id);
                            tempAd.Address = reader.IsDBNull(reader.GetOrdinal("Address")) ? string.Empty : (string)reader["Address"];

                            result.Add(tempAd);
                        }
                    }
                }
            }
            return result;
        }

        public List<Ad> GetAds(AdFilter filter, int? pageIndex = 1, int? adsPerPage = 6)
        {

            List<Ad> result = new List<Ad>();
            //FREAKY COSTYL
            if (filter == null)
            {
                filter = new AdFilter();
                filter.IsRequest = null;
                filter.IsEnabled = null;
                filter.IsActive = null;
                filter.AuthorId = null;
                filter.Town = null;
                filter.Start = null;
                filter.Finish = null;
            }
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspGetAds", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@IsRequest", filter.IsRequest ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@IsEnabled", filter.IsEnabled ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@PageIndex", pageIndex);
                    command.Parameters.AddWithValue("@AdsPerPage", adsPerPage);
                    command.Parameters.AddWithValue("@AuthorId", filter.AuthorId ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@Town", filter.Town ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@Start_Time", filter.Start ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@End_Time", filter.Finish ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@Active", filter.IsActive ?? (object)DBNull.Value);
                    con.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Ad tempAd = new Ad();
                            tempAd.Id = (int)reader["Id"];
                            tempAd.IsRequest = (bool)reader["IsRequest"];
                            tempAd.IsEnabled = (bool)reader["IsEnabled"];
                            tempAd.IsActive = (bool)reader["IsActive"];
                            tempAd.Title = (string)reader["Title"];
                            tempAd.Description = (string)reader["Description"];
                            tempAd.BeginTime = (DateTime)reader["Begin_date"];
                            tempAd.EndTime = (DateTime)reader["End_date"];
                            tempAd.Country = (string)reader["Country"];
                            tempAd.Town = (string)reader["Town"];
                            tempAd.Address = (string)reader["Address"] ?? "";
                            tempAd.Views = this.CountAdViewing(tempAd.Id);
                            tempAd.Subscribers = this.GetAdSubscribers(tempAd.Id);
                            AdAuthor tempAuthor = new AdAuthor();
                            tempAuthor.Id = (int)reader["AuthorId"];
                            tempAuthor.FirstName = (string)reader["AuthorName"];
                            tempAuthor.SurName = reader.IsDBNull(reader.GetOrdinal("AuthorSurname")) ? string.Empty : (string)reader["AuthorSurname"];
                            tempAuthor.Email = (string)reader["AuthorEmail"];
                            tempAuthor.Town = reader.IsDBNull(reader.GetOrdinal("AuthorTown")) ? string.Empty : (string)reader["AuthorTown"];
                            DateTime birthDate = (DateTime)reader["AuthorBirth"];
                            DateTime todayDate = DateTime.Today;
                            TimeSpan timeSpan = (TimeSpan)(todayDate - birthDate);
                            tempAuthor.Age = (int)(timeSpan.TotalDays / 365.0);
                            tempAd.Author = tempAuthor;

                            result.Add(tempAd);
                        }
                    }

                }
            }
            return result;
        }

        public List<Ad> GetAdsMod(AdFilter filter, int StartRowIndex, int MaximumRows)
        {
            int pageIndex = (int)(StartRowIndex / MaximumRows) + 1;
            return GetAds(filter, pageIndex, MaximumRows);
        }

        public int CountSuitableAds(AdFilter filter)
        {
            if (filter == null)
            {
                filter = new AdFilter();
                filter.IsRequest = null;
                filter.IsEnabled = null;
                filter.AuthorId = null;
                filter.IsActive = null;
                filter.Town = null;
                filter.Start = null;
                filter.Finish = null;
            }

            int result = 0;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspCountSuitableAds", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@IsRequest", filter.IsRequest ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@IsEnabled", filter.IsEnabled ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@AuthorId", filter.AuthorId ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@Town", filter.Town ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@Start_Time", filter.Start ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@End_Time", filter.Finish ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@Active", filter.IsActive ?? (object)DBNull.Value);
                    con.Open();
                    result = (int)command.ExecuteScalar();
                }
            }
            return result;
        }

        public int CountSuitableAdsMod(AdFilter filter, int StartRowIndex, int MaximumRows)
        {
            return CountSuitableAds(filter);
        }

        public int CountAdsByType(bool isRequest)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                string query = SELECT_ALL_ADS_BY_TYPE;
                using (SqlCommand command = new SqlCommand(query, con))
                {
                    con.Open();
                    command.Parameters.AddWithValue("IsRequest", isRequest);
                    return (int)command.ExecuteScalar();
                }
            }
        }

        public int CreateAd(Ad toAdd)
        {
            int adId = -1;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(INSERT_NEW_AD, con))
                {
                    con.Open();
                    command.Parameters.AddWithValue("@IsRequest", toAdd.IsRequest);
                    command.Parameters.AddWithValue("@Title", toAdd.Title);
                    command.Parameters.AddWithValue("@IsEnabled", true);
                    command.Parameters.AddWithValue("@AuthorId", toAdd.Author.Id);
                    command.Parameters.AddWithValue("@Description", toAdd.Description);
                    command.Parameters.AddWithValue("@Begin_date", toAdd.BeginTime);
                    command.Parameters.AddWithValue("@End_date", toAdd.EndTime);
                    command.Parameters.AddWithValue("@Country", toAdd.Country);
                    command.Parameters.AddWithValue("@Town", toAdd.Town);
                    command.Parameters.AddWithValue("@Address", toAdd.Address ?? "");
                    command.Parameters.AddWithValue("@Active", true);

                    adId = (Int32)command.ExecuteScalar();
                }
            }
            return adId;
        }

        public void DeleteAd(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(DELETE_AD, con))
                {
                    command.Parameters.AddWithValue("@id", Id);
                    command.ExecuteNonQuery();
                }
                using (SqlCommand command = new SqlCommand(DELETE_AD_COMMENT_BY_USER_ID, con))
                {
                    command.Parameters.AddWithValue("@UserId", Id);
                    command.ExecuteNonQuery();
                }
            }
        }

        public int AddComment(AdComment comment)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(INSERT_AD_COMMENT, con))
                {
                    con.Open();
                    command.Parameters.AddWithValue("@AdId", comment.AdId);
                    command.Parameters.AddWithValue("@IsEnabled", true);
                    command.Parameters.AddWithValue("@UserId", comment.UserId ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@Time", comment.Time);
                    command.Parameters.AddWithValue("@Comment", comment.Comment);

                    return (Int32)command.ExecuteScalar();
                }
            }
        }

        public List<AdComment> GetAdComments(int adId)
        {
            List<AdComment> comments = new List<AdComment>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspGetCommentsByAdId", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Id", adId);
                    con.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AdComment adComment = new AdComment();

                            adComment.Id = (int)reader["Id"];
                            adComment.IsEnabled = (bool)reader["IsEnabled"];
                            adComment.AdId = (int)reader["AdId"];
                            adComment.UserId = reader.IsDBNull(reader.GetOrdinal("UserId")) ? 0 : (int)reader["UserId"];
                            adComment.UserName = reader.IsDBNull(reader.GetOrdinal("Firstname")) ? "Guest" : (string)reader["Firstname"];
                            adComment.Time = (DateTime)reader["Time"];
                            adComment.Comment = (string)reader["Comment"];

                            comments.Add(adComment);
                        }
                    }
                }
                return comments;
            }
        }

        public List<AdComment> GetAllAdComments()
        {
            List<AdComment> comments = new List<AdComment>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(SELECT_ALL_COMMENTS, con))
                {            
                    con.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AdComment adComment = new AdComment();

                            adComment.Id = (int)reader["Id"];
                            adComment.IsEnabled = (bool)reader["IsEnabled"];
                            adComment.AdId = (int)reader["AdId"];
                            adComment.UserId = reader.IsDBNull(reader.GetOrdinal("UserId")) ? 0 : (int)reader["UserId"];
                            adComment.Time = (DateTime)reader["Time"];
                            adComment.Comment = (string)reader["Comment"];

                            comments.Add(adComment);
                        }
                    }
                }
            }
            return comments;
        }

        public int CountAdViewing(int adId)
        {
            int views = 0;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspCountAdViewers", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@AdViewingId", adId);
                    con.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            views = (int)reader["AdViewing"];
                        }
                    }
                }
                return views;
            }
        }


        public void UpdateAdStatus(int Id, bool IsEnabled)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspUpdateAdEnableStatus", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Id", Id);
                    command.Parameters.AddWithValue("@Enable", IsEnabled);
                    con.Open();
                    command.ExecuteNonQuery();
                }

            }
        }

        public void UpdateAdActiveStatus(int Id, bool isActive)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspUpdateAdActiveStatus", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Id", Id);
                    command.Parameters.AddWithValue("@Active", isActive);
                    con.Open();
                    command.ExecuteNonQuery();
                }

            }
        }

        public void UpdateAdComment(int Id, bool IsEnabled)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspUpdateAdCommentStatus", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Id", Id);
                    command.Parameters.AddWithValue("@Status", IsEnabled);
                    con.Open();
                    command.ExecuteNonQuery();
                }

            }
        }
        public void InsertViews(int UserId, int AdId)
        {
            int isViewed = 0;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(SELECT_VIEW, con))
                {
                    con.Open();
                    command.Parameters.AddWithValue("@userId", UserId);
                    command.Parameters.AddWithValue("@adId", AdId);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            isViewed = reader.IsDBNull(reader.GetOrdinal("Id")) ? 0 : (int)reader["Id"];
                        }
                    }
                }

                if (isViewed == 0)
                {
                    using (SqlCommand command = new SqlCommand(INSERT_NEW_VIEW, con))
                    {
                        command.Parameters.AddWithValue("@AdId", AdId);
                        command.Parameters.AddWithValue("@UserId", UserId);
                        command.ExecuteNonQuery();
                    }
                }
            }
        }


        public void DeleteAdComment(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(DELETE_AD_COMMENT_BY_ID, con))
                {
                    con.Open();
                    command.Parameters.AddWithValue("@id", Id);
                    command.ExecuteNonQuery();
                }
            }
        }

        public List<AdSubscribers> GetAdSubscribers(int adId)
        {
            List<AdSubscribers> subscribers = new List<AdSubscribers>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspGetSubscribersByAdId", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Id", adId);
                    con.Open();


                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AdSubscribers subs = new AdSubscribers();

                            subs.Id = (int)reader["Id"];
                            subs.AdId = (int)reader["AdId"];
                            subs.UserId = (int)reader["UserId"]; ;
                            subs.Time = (DateTime)reader["Date"];
                            subs.Name = (string)reader["Name"];

                            subscribers.Add(subs);
                        }
                    }
                }
            }
            return subscribers;
        }

        public bool SubscribeOnAd(int UserId, int AdId)
        {
            List<AdSubscribers> subscribers = this.GetAdSubscribers(AdId);
            bool result = false;

            if (subscribers == null || subscribers.Count <= 0)
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    using (SqlCommand command = new SqlCommand(INSERT_NEW_SUBSCRIBERS, con))
                    {
                        con.Open();
                        command.Parameters.AddWithValue("@AdId", AdId);
                        command.Parameters.AddWithValue("@UserId", UserId);
                      //  command.Parameters.AddWithValue("@Date", DateTime.Now.ToShortDateString(""));
                        command.ExecuteNonQuery();

                        result = true;
                    }
                }
            }
            return result;
        }

        #endregion
    }
}
