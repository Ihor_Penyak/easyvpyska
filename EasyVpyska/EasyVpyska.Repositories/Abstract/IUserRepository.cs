﻿using EasyVpyska.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.Repositories.Abstract
{
    public interface IUserRepository
    {
        int GetCount(string text, int? status);

        int GetUsersCount();

        bool IsLoginExist(string login);
        bool IsEmailExist(string email);
        bool IsPhoneExist(string phone);

        int GetUserId(string login);

        User GetUser(string login);

        User GetUserById(int id);

        int GetUserAge(int id);

        string[] GetAllRoles();

        string[] GetRolesForUser(string login);

        string[] GetUsersInRole(string rolename);

        int GetRoleId(string rolename);

        bool IsUserInRole(string login, string rolename);

        bool IsUserEnabled(string login);

        bool IsRoleExists(string rolename);

        List<User> SelectAll(int i, int j, string filter, int? status);

        void UpdateUser(int Id, bool Status);

        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="password">User password</param>
        /// <returns>Is user added to DataBase</returns>
        int CreateUser(User user, string password);
        int UpdateUser(User user, string password);
        string GetUserHash(string login);
        int UploadPhoto(ProfileImage image);
        ProfileImage LoadPhoto(int userId);
        int UpdatePhoto(ProfileImage image);
        bool IsProfilePhotoExists(int userId);

    }
}
