﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Frontend.Controllers;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;

namespace EasyVpyska.WebUI.Frontend.Test
{
    [TestClass]
    public class LayoutTest
    {
        [TestMethod]
        public void PartialAuthenticatedTest()
        {
            var security = new Mock<ISecurityManager>();       
                        
            security.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            security.Setup(a => a.GetCurrentUserId()).Returns(1);
            LayoutController controller = new LayoutController(security.Object);

            // Act
            PartialViewResult result = controller.PartialAuthenticated() as PartialViewResult;

         
            // Assert
            Assert.AreEqual(true, result.ViewData["UserLog"]);
            Assert.AreEqual(1, result.ViewData["loginUsersId"]);
            Assert.AreEqual("PartialAuthenticated", result.ViewName);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PartialLogOutTest()
        {
            var security = new Mock<ISecurityManager>();            

            security.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);            
            LayoutController controller = new LayoutController(security.Object);

            // Act
            PartialViewResult result = controller.PartialLogOut() as PartialViewResult;

            // Assert
            Assert.AreEqual(true, result.ViewData["UserLog"]);
            Assert.AreEqual("PartialLogOut", result.ViewName);
            Assert.IsNotNull(result);
        }
    }
}
