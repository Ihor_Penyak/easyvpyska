﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Frontend.Controllers;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Security.Principal;

namespace EasyVpyska.WebUI.Frontend.Test
{
    [TestClass]
    public class CreateAdTest
    {
        Mock<IAdRepository> _ad = new Mock<IAdRepository>();
        Mock<ISecurityManager> _security = new Mock<ISecurityManager>();
        Mock<IUserRepository> _user = new Mock<IUserRepository>();
        Mock<IEmailService> _mail = new Mock<IEmailService>();        

        [TestMethod]
        public void CreateAdIndexTest()
        {          
            _security.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            _security.Setup(a => a.GetCurrentUserId()).Returns(2);
            
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateAdIndexSecondTest()
        {
            _security.Setup(a => a.IsCurrentUserAuthenticated()).Returns(false);            

            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateTest()
        {           
            string ad= "{\"authorId\":\"1\",\"Title\":\"Adtitle\",\"Description\":\"dddddescription\",\"Address\":\"nope\",\"BeginTime\":\"02.02.2015\",\"EndTime\":\"03.02.2015\",\"Town\":\"Vinnica\",\"Country\":\"Ukraine\"}";
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ContentResult result = controller.Create(ad) as ContentResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateWithRegisterTest()
        {
            string ad = "{\"AuthorId\":\"2\",\"Title\":\"Adtitle\",\"Description\":\"dddddescription\",\"Address\":\"Zelena, 5\",\"BeginTime\":\"02.02.2016\",\"EndTime\":\"03.02.2016\",\"Town\":\"Vinnica\",\"Country\":\"Ukraine\"}";
            string userJson = "{\"FirstName\":\"Petr\",\"Login\":\"voycikhovsky@mail.ru\",\"DateOfBirth\":\"01/13/1958\",\"Email\":\"voycikhovsky@mail.ru\",\"Phone\":\"+380995156814\",\"Country\":\"Ukraine\",\"Town\":\"Vinnica\",\"Address\":\"Наливайченка 3\",\"About\":\"Не хроплю!\"}";
                      
            _security.Setup(a => a.GetUserId(It.IsAny<string>())).Returns(2);
            _user.Setup(a => a.GetUserId(It.IsAny<string>())).Returns(2);
            _ad.Setup(a => a.CreateAd(It.IsAny<Ad>())).Returns(1); 
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ContentResult result = controller.CreateWithRegister(ad, userJson) as ContentResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateWithRegisterFailTest()
        {
            string ad = "{\"AuthorId\":\"\",\"Title\":\"\",\"Description\":\"\",\"Address\":\"\",\"BeginTime\":\"\",\"EndTime\":\"\",\"Town\":\"\",\"Country\":\"\"}";
            string userJson = "{\"FirstName\":\"\",\"Login\":\"\",\"DateOfBirth\":\"\",\"Email\":\"\",\"Phone\":\"\",\"Country\":\"\",\"Town\":\"\",\"Address\":\"3\",\"About\":\"Не\"}";
                       
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ContentResult result = controller.CreateWithRegister(ad, userJson) as ContentResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateWithRegisterFailSecondTest()
        {
            string ad = "{\"AuthorId\":\"\",\"Title\":\"\",\"Description\":\"\",\"Address\":\"\",\"BeginTime\":\"02.02.2016\",\"EndTime\":\"01.01.2016\",\"Town\":\"\",\"Country\":\"\"}";
            string userJson = "{\"FirstName\":\"tyttgdcdxxfexrcrcfvggfvtdfrederddrffvghb hjkmnygftrdfdrederdeededdecbhnhjmmhyfresdsweswesesxdcfvgbhjnjktfvgbhnjcfvbj\",\"Login\":\"122mail\",\"DateOfBirth\":\"01/13/2015\",\"Email\":\"voycikhovsky@mail.ru\",\"Phone\":\"\",\"Country\":\"\",\"Town\":\"\",\"Address\":\"3\",\"About\":\"Не\"}";

            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ContentResult result = controller.CreateWithRegister(ad, userJson) as ContentResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateWithRegisterFailEmailTest()
        {
            string ad = "{\"AuthorId\":\"\",\"Title\":\"\",\"Description\":\"\",\"Address\":\"\",\"BeginTime\":\"02.02.2016\",\"EndTime\":\"01.01.2016\",\"Town\":\"\",\"Country\":\"Ukraine\"}";
            string userJson = "{\"FirstName\":\"ty\",\"Login\":\"voycikhovsky@mail.ru\",\"DateOfBirth\":\"01/13/2015\",\"Email\":\"voycikhovsky@mail.ru\",\"Phone\":\"\",\"Country\":\"\",\"Town\":\"\",\"Address\":\"3\",\"About\":\"Не\"}";

            _user.Setup(a => a.IsEmailExist(It.IsAny<string>())).Returns(true);
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ContentResult result = controller.CreateWithRegister(ad, userJson) as ContentResult;

            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void CreateWithRegisterExceprionTest()
        {
            string ad = "{\"AuthorId\":\"2\",\"Title\":\"Adtitle\",\"Description\":\"dddddescription\",\"Address\":\"Zelena, 5\",\"BeginTime\":\"02.02.2016\",\"EndTime\":\"03.02.2016\",\"Town\":\"Vinnica\",\"Country\":\"Ukraine\"}";
            string userJson = "{\"FirstName\":\"Petr\",\"Login\":\"voycikhovsky@mail.ru\",\"DateOfBirth\":\"01/13/1958\",\"Email\":\"voycikhovsky@mail.ru\",\"Phone\":\"+380995156814\",\"Country\":\"Ukraine\",\"Town\":\"Vinnica\",\"Address\":\"Наливайченка 3\",\"About\":\"Не хроплю!\"}";

            _mail.Setup(a => a.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws<Exception>();
 
            _security.Setup(a => a.GetUserId(It.IsAny<string>())).Returns(2);
            _user.Setup(a => a.GetUserId(It.IsAny<string>())).Returns(2);
            _ad.Setup(a => a.CreateAd(It.IsAny<Ad>())).Returns(1);
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ContentResult result = controller.CreateWithRegister(ad, userJson) as ContentResult;

            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void CreateWithRegisterSecondTest()
        {
            string ad = "{\"authorId\":\"1\",\"Title\":\"AdtitlAdtitleAdtitleAdtitleAdtitleAdtitleAdtitlee\",\"Description\":\"dn\",\"Address\":\"nope\",\"BeginTime\":\"10.03.2015\",\"EndTime\":\"03.02.2015\",\"Town\":\"ца\",\"Country\":\"Ukrаина\"}";
            string userJson = "{\"FirstName\":\"Petr5\",\"SurName\":\"Voycikho5ky\",\"DateOfBirth\":\"01/13/1800\",\"Email\":\"voycikhovsky@mailru\",\"Phone\":\"0995156814\",\"Country\":\"Ukrain7\",\"Town\":\"Vinца\",\"Address\":\"Наливайченка 3\",\"About\":\"\"}";
            
            _user.Setup(a => a.GetUserId(It.IsAny<string>())).Returns(2);
            
            _ad.Setup(a => a.CreateAd(It.IsAny<Ad>())).Returns(1);
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ContentResult result = controller.CreateWithRegister(ad, userJson) as ContentResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateSecondTest()
        {
            string ad = "{\"authorId\":\"1\",\"Title\":\"Adtitle\",\"Description\":\"dddddescription\",\"Address\":\"Zelena, 5\",\"BeginTime\":\"02.02.2016\",\"EndTime\":\"03.03.2016\",\"Town\":\"Vinnica\",\"Country\":\"Ukraine\"}";
          
            _ad.Setup(a => a.CreateAd(It.IsAny<Ad>())).Returns(1);
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            ContentResult result = controller.Create(ad) as ContentResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void EmailTest()
        {            
            _user.Setup(a => a.IsEmailExist(It.IsAny<string>())).Returns(true);
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            JsonResult result = controller.IsEmailAwaliable("email") as JsonResult;

            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void EmailFailTest()
        {
            _user.Setup(a => a.IsEmailExist(It.IsAny<string>())).Returns(false);
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            JsonResult result = controller.IsEmailAwaliable("email") as JsonResult;

            Assert.IsNotNull(result);
        }

        
          [TestMethod]
          [ExpectedException(typeof(ArgumentNullException))]
        public void EmailExceptionTest()
        {         
            CreateAdController controller = new CreateAdController(_user.Object, _security.Object, _ad.Object, _mail.Object);

            JsonResult result = controller.IsEmailAwaliable(null) as JsonResult;
              
          }
    }
}
