﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Frontend.Controllers;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Text;

namespace EasyVpyska.WebUI.Frontend.Test
{
    [TestClass]
    public class AdDetailsTest
    {

        Mock<IImageService> _image = new Mock<IImageService>();
        Mock<IUserRepository> _userMock = new Mock<IUserRepository>();
        Mock<IAdRepository> _adMock = new Mock<IAdRepository>();
        Mock<ISecurityManager> _secure = new Mock<ISecurityManager>();
        Mock<IEmailService> _email = new Mock<IEmailService>();
         
        
        [TestMethod]
        public void DetailsIndexTest()
        {
            byte[] img = new byte[] { 0, 1, 5, 2 };

                 Ad ad = new Ad();
                ad.Id = 1;
                AdAuthor author = new AdAuthor();
                author.Id = 1;
                ad.Author = author;
                ad.IsEnabled = true;
                ad.Subscribers = new List<AdSubscribers>();
                ad.Subscribers.Add(new AdSubscribers { UserId = 2 });

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            _adMock.Setup(a => a.InsertViews(It.IsAny<int>(), It.IsAny<int>()));
            _secure.Setup(a=> a.IsCurrentUserAuthenticated()).Returns(true);
            _secure.Setup(a => a.GetCurrentUserId()).Returns(1);          
            _image.Setup(a => a.LoadPhoto(It.IsAny<int>())).Returns(new ProfileImage() { File = img });
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            // Act
            ViewResult result = controller.Index("1") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DetailsIndexImgTest()
        {
            Ad ad = new Ad();
            ad.Id = 1;
            AdAuthor author = new AdAuthor();
            author.Id = 1;
            ad.Author = author;
            ad.IsEnabled = true;
            ad.Subscribers = new List<AdSubscribers>();
            ad.Subscribers.Add(new AdSubscribers { UserId = 1 });

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            _adMock.Setup(a => a.InsertViews(It.IsAny<int>(), It.IsAny<int>()));
            _secure.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            _secure.Setup(a => a.GetCurrentUserId()).Returns(1);
            _image.Setup(a => a.LoadPhoto(It.IsAny<int>())).Returns(new ProfileImage() { File = null });
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            // Act
            ViewResult result = controller.Index("1") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void DetailsIndexErrorTest()
        {                        
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            // Act
            ViewResult result = controller.Index("") as ViewResult;
                       
        }

        [TestMethod]
        public void GetAddressesTest()
        {      
            Ad ad = new Ad();
            ad.Id = 1;
            AdAuthor author = new AdAuthor();
            author.Id = 1;
            ad.Author = author;
            ad.Town = "Lviv";            
            ad.IsEnabled = true;
            ad.Subscribers = new List<AdSubscribers>();

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);
            // Act
            JsonResult result = controller.GetAddresses(1) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetAdInfoTest()
        {
            Ad ad = new Ad();
            ad.Id = 1;
            AdAuthor author = new AdAuthor();
            author.Id = 1;
            ad.Author = author;
            ad.IsEnabled = true;
            ad.Subscribers = new List<AdSubscribers>();

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            // Act
            JsonResult result = controller.GetAdInfo(1) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void UserSubscribedTest()
        {
            Ad ad = new Ad();
            ad.Id = 1;
            ad.Title = "123";
            AdAuthor author = new AdAuthor();
            author.Id = 1;
            ad.Author = author;
            ad.IsEnabled = true;
            ad.Subscribers = new List<AdSubscribers>();

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            _adMock.Setup(a => a.SubscribeOnAd(It.IsAny<int>(), It.IsAny<int>())).Returns(true);
            _secure.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            _secure.Setup(a => a.GetCurrentUserId()).Returns(1);     
         
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            // Act
            JsonResult result = controller.UserSubscribed(1) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddCommentTest()
        {        

            Ad ad = new Ad();
            ad.Id = 1;
            AdAuthor author = new AdAuthor();
            author.Id = 1;
            ad.Author = author;
            ad.Author = new AdAuthor() { Email="456"};
            ad.IsEnabled = true;
            ad.Subscribers = new List<AdSubscribers>();

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            _adMock.Setup(a => a.AddComment(It.IsAny<AdComment>())).Returns(1); ;
            _secure.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            _secure.Setup(a => a.GetCurrentUserId()).Returns(1);
            _email.Setup(a => a.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            _userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(new User() {FirstName="Ivan" });   
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            // Act
            JsonResult result = controller.AddComment(1, "123") as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void AddCommentsTest()
        {

            Ad ad = new Ad();
            ad.Id = 1;
            AdAuthor author = new AdAuthor();
            author.Id = 1;
            ad.Author = author;
            ad.Author = new AdAuthor() { Email = "456" };
            ad.IsEnabled = true;
            ad.Subscribers = new List<AdSubscribers>();

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            _adMock.Setup(a => a.AddComment(It.IsAny<AdComment>())).Returns(1); ;
            _secure.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            _secure.Setup(a => a.GetCurrentUserId()).Returns(1);
            _email.Setup(a => a.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            _userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(new User() { FirstName = "Ivan" });
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            // Act
            JsonResult result = controller.AddComment(1,null) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddCommentErrorTest()
        {

            Ad ad = new Ad();
            ad.Id = 1;
            AdAuthor author = new AdAuthor();
            author.Id = 1;
            ad.Author = author;
            ad.Author = new AdAuthor() { Email = "456" };
            ad.IsEnabled = true;
            ad.Subscribers = new List<AdSubscribers>();
            

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            _adMock.Setup(a => a.AddComment(It.IsAny<AdComment>())).Returns(1); ;
            _secure.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            _secure.Setup(a => a.GetCurrentUserId()).Returns(1);
            _email.Setup(a => a.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            _userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(new User() { FirstName = "Ivan" });
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            // Act
            JsonResult result = controller.AddComment(1, "") as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void AddCommentFailTest()
        {

            Ad ad = new Ad();
            ad.Id = 1;
            AdAuthor author = new AdAuthor();
            author.Id = 1;
            ad.Author = author;
            ad.Author = new AdAuthor() { Email = "456" };
            ad.IsEnabled = true;
            ad.Subscribers = new List<AdSubscribers>();

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            _adMock.Setup(a => a.AddComment(It.IsAny<AdComment>())).Returns(1); ;
            _secure.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            _secure.Setup(a => a.GetCurrentUserId()).Returns(1);
            _email.Setup(a => a.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            _userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(new User() { FirstName = "Ivan" });
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            StringBuilder str = new StringBuilder();

            for (int i = 0; i < 510; i++)
            {
                str.Append("a");            
            }
            string message = str.ToString();
            // Act
            JsonResult result = controller.AddComment(1, message) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddCommentFailedTest()
        {
            Ad ad = new Ad();
            ad.Id = 1;
            AdAuthor author = new AdAuthor();
            author.Id = 1;
            ad.Author = author;
            ad.Author = new AdAuthor() { Email = "456" };
            ad.IsEnabled = true;
            ad.Subscribers = new List<AdSubscribers>();

            _adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(ad);
            _adMock.Setup(a => a.AddComment(It.IsAny<AdComment>())).Returns(1); ;
            _secure.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            _secure.Setup(a => a.GetCurrentUserId()).Returns(1);
            _email.Setup(a => a.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws<Exception>(); 
            _userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(new User() { FirstName = "Ivan" });
            AdDetailsController controller = new AdDetailsController(_adMock.Object, _secure.Object, _email.Object, _userMock.Object, _image.Object);

            // Act
            JsonResult result = controller.AddComment(1, "123") as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
