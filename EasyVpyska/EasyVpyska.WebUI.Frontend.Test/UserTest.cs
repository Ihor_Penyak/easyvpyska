﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Frontend.Controllers;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;

namespace EasyVpyska.WebUI.Frontend.Test
{
    [TestClass]
    public class UserTest
    {
        [TestMethod]
        public void UserIndexTest()
        {
            var userMock = new Mock<IUserRepository>();
            var adMock = new Mock<IAdRepository>();
            var security = new Mock<ISecurityManager>();
            byte[] img = new byte[] {0,1,5,2 };

            userMock.Setup(a => a.GetUserById(It.Is<int>(i => i > 0))).Returns(new User() { Id = 1 });
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = img });
            security.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            UserController controller = new UserController(userMock.Object, security.Object, adMock.Object);

            // Act
            ViewResult result = controller.Index("1") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void UserIndexNullImageTest()
        {
            var userMock = new Mock<IUserRepository>();
            var adMock = new Mock<IAdRepository>();
            var security = new Mock<ISecurityManager>();

            userMock.Setup(a => a.GetUserById(It.Is<int>(i => i > 0))).Returns(new User() { Id = 1 });
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = null });
         
            UserController controller = new UserController(userMock.Object, security.Object, adMock.Object);

            // Act
            ViewResult result = controller.Index("1") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

          [TestMethod]
          [ExpectedException(typeof(ArgumentException))]
        public void IndexErrorTest()
        {
            var userMock = new Mock<IUserRepository>();
            var adMock = new Mock<IAdRepository>();
            var security = new Mock<ISecurityManager>();
                       
            UserController controller = new UserController(userMock.Object, security.Object, adMock.Object);

            // Act
            ViewResult result = controller.Index("abc") as ViewResult;       
         
        }


          [TestMethod]
          public void AllAdsTest()
          {
              var userMock = new Mock<IUserRepository>();
              var adMock = new Mock<IAdRepository>();
              var security = new Mock<ISecurityManager>();

             
              security.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
              security.Setup(a => a.GetCurrentUserId()).Returns(0);
             
              adMock.Setup(a => a.GetAds(It.IsAny<AdFilter>(), 1, 6)).Returns(new List<Ad>() { new Ad() });
             
              UserController controller = new UserController(userMock.Object, security.Object, adMock.Object);

              // Act
              PartialViewResult result = controller.AllAds(2, It.Is<int>(i => i > 0)) as PartialViewResult;

              // Assert
              Assert.IsNotNull(result);
          }

          [TestMethod]
          public void AllAdsSecondTest()
          {
              var userMock = new Mock<IUserRepository>();
              var adMock = new Mock<IAdRepository>();
              var security = new Mock<ISecurityManager>();


              security.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
              security.Setup(a => a.GetCurrentUserId()).Returns(1);

              adMock.Setup(a => a.GetAds(It.IsAny<AdFilter>(), 1, 6)).Returns(new List<Ad>() { new Ad() });

              UserController controller = new UserController(userMock.Object, security.Object, adMock.Object);

              // Act
              PartialViewResult result = controller.AllAds(1, It.Is<int>(i => i > 0)) as PartialViewResult;

              // Assert
              Assert.IsNotNull(result);
          }

          [TestMethod]
          public void SetEnableAdTest()
          {
              var userMock = new Mock<IUserRepository>();
              var adMock = new Mock<IAdRepository>();
              var security = new Mock<ISecurityManager>();
              
              adMock.Setup(a => a.UpdateAdStatus(1,true));
              
              UserController controller = new UserController(userMock.Object, security.Object, adMock.Object);

              // Act
              PartialViewResult result = controller.SetActiveAd(It.Is<int>(i => i > 0), true) as PartialViewResult;
              
              // Assert
              Assert.IsNotNull(result);
          }

          [TestMethod]
          public void DeleteAdTest()
          {
              var userMock = new Mock<IUserRepository>();
              var adMock = new Mock<IAdRepository>();
              var security = new Mock<ISecurityManager>();
              
              adMock.Setup(a => a.DeleteAd(1));

              UserController controller = new UserController(userMock.Object, security.Object, adMock.Object);

              // Act
              PartialViewResult result = controller.DeleteAd(It.Is<int>(i => i > 0)) as PartialViewResult;

              // Assert
              Assert.IsNotNull(result);
          }


          [TestMethod]
          public void SearchSuitableAdsTest()
          {
              var userMock = new Mock<IUserRepository>();
              var adMock = new Mock<IAdRepository>();
              var security = new Mock<ISecurityManager>();


              adMock.Setup(a => a.GetAd(It.Is<int>(i => i > 0))).Returns(new Ad());
              adMock.Setup(a => a.GetAds(It.IsAny<AdFilter>(), 1, 6)).Returns(new List<Ad>() { new Ad() });

              UserController controller = new UserController(userMock.Object, security.Object, adMock.Object);

              // Act
              PartialViewResult result = controller.SearchSuitableAds(1) as PartialViewResult;

              // Assert
              Assert.IsNotNull(result);
          }
    }
}
