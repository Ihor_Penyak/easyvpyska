﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using BCrypt.Net;

using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Entities;

namespace EasyVpyska.WebUI
{
    public class SecurityManager : ISecurityManager
    {
        #region Fields

        private IUserRepository _userRepository;

        #endregion

        #region Constructors

        public SecurityManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        #endregion

        #region Methods

        public bool Authentication(string login, string password)
        {
            return BCrypt.Net.BCrypt.Verify(password, _userRepository.GetUserHash(login));
        }

        public bool IsCurrentUserAuthenticated()
        {
            return ((HttpContext.Current.User != null)
                && (HttpContext.Current.User.Identity != null)
                && HttpContext.Current.User.Identity.IsAuthenticated);
        }

        public string GetUserRole(string login)
        {
            return _userRepository.GetUser(login).Role;
        }

        public int GetCurrentUserId()
        {
            return _userRepository.GetUserId(HttpContext.Current.User.Identity.Name);
        }

        public string GetCurrentUserName()
        {
            return HttpContext.Current.User.Identity.Name;
        }

        public int GetUserId(string login)
        {
            return _userRepository.GetUserId(login);
        }

        public bool IsUserInRole(string login, string role)
        {
            return Roles.IsUserInRole(login, role);
        }

        public bool IsUserEnabled(string login)
        {
            return _userRepository.IsUserEnabled(login);
        }

        public bool RoleExists(string role)
        {
            return Roles.RoleExists(role);
        }

        public void RedirectFromLoginPage(string login, bool createPersistanceCokie)
        {
            FormsAuthentication.RedirectFromLoginPage(login, createPersistanceCokie);
        }

        public void SetAuthCookie(string name, bool createPersistanceCokie)
        {
            FormsAuthentication.SetAuthCookie(name, createPersistanceCokie);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        public void RedirectToLoginPage()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public int UpdateUser(User user, string password)
        {
            if (password != "")
            {
                password = BCrypt.Net.BCrypt.HashPassword(password);
            }

            return _userRepository.UpdateUser(user, password);
        }

        public int RegistrateUser(User user, string password)
        {
            var hashedPass = BCrypt.Net.BCrypt.HashPassword(password);
            return _userRepository.CreateUser(user, hashedPass);
        }

        #endregion
    }
}