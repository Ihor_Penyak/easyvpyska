﻿using System.Web.Security;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Globalization;

using EasyVpyska.Entities;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;


namespace EasyVpyska.WebUI
{
    public class EasyVpyskaRoleProvider : RoleProvider
    {
        #region Fields

        private IUserRepository _userRepository;

        private string _eventSource = "SqlRoleProvider";
        private string _eventLog = "Application";
        private string _exceptionMessage = "An exception occurred. Please check the Event Log.";
        private string pApplicationName;
        private ConnectionStringSettings pConnectionStringSettings;
        private string connectionString;

        #endregion

        #region Properties

        public override string ApplicationName
        {
            get { return pApplicationName; }
            set { pApplicationName = value; }
        }


        #endregion

        #region Inititalization

        public override void Initialize(string name, NameValueCollection config)
        {

            // 
            // Initialize values from web.config. 
            // 

            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "SqlRoleProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Sample Sql Role provider");
            }

            // Initialize the abstract base class. 
            base.Initialize(name, config);


            if (config["applicationName"] == null || config["applicationName"].Trim() == "")
            {
                pApplicationName = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
            }
            else
            {
                pApplicationName = config["applicationName"];
            }

            pConnectionStringSettings = ConfigurationManager.
                ConnectionStrings[config["connectionStringName"]];

            if (pConnectionStringSettings == null || pConnectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new ProviderException("Connection string cannot be blank.");
            }

            connectionString = pConnectionStringSettings.ConnectionString;

            _userRepository = new UserRepository(connectionString);
        } 

        #endregion

        #region Methods

        public override string[] GetAllRoles()
        {
            return _userRepository.GetAllRoles();
        }

        public override string[] GetRolesForUser(string username)
        {
            return _userRepository.GetRolesForUser(username);
        }

        public override string[] GetUsersInRole(string rolename)
        {
            return _userRepository.GetUsersInRole(rolename);
        }

        public override bool IsUserInRole(string username, string rolename)
        {
            return _userRepository.IsUserInRole(username, rolename);
        }

        public override bool RoleExists(string rolename)
        {
            return _userRepository.IsRoleExists(rolename);
        } 

        #endregion

        #region NotImplemented Methods

        public override void AddUsersToRoles(string[] usernames, string[] rolenames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string rolename)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string rolename, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] rolenames)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string rolename, string usernameToMatch)
        {
            throw new NotImplementedException();
        } 

        #endregion
    }
}