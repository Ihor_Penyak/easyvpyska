﻿using EasyVpyska.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EasyVpyska.WebUI
{
    public class RequestService : IRequestService
    {

        public string ReturnPath(Controller controller)
        {
            return controller.Request.Url.PathAndQuery;        
        }

        public bool CheckAjaxRequest(Controller controller)
        {
            return controller.Request.IsAjaxRequest();
        }
    }
}
