﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

using EasyVpyska.Entities;

namespace EasyVpyska.WebUI.Abstract
{
    public interface ISecurityManager
    {
        bool Authentication(string login, string password);
        bool IsCurrentUserAuthenticated();
        bool IsUserInRole(string login, string role);
        bool IsUserEnabled(string login);
        bool RoleExists(string role);

        int RegistrateUser(User user, string password);

        string GetUserRole(string login);
        int GetCurrentUserId();
        string GetCurrentUserName();
        int GetUserId(string login);
        void RedirectFromLoginPage(string login, bool createPersistanceCokie);
        void SetAuthCookie(string name, bool createPersistanceCokie);
        void SignOut();
        void RedirectToLoginPage();
        int UpdateUser(User user, string password);

    }
}
