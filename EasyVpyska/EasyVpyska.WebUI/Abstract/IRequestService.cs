﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EasyVpyska.Repositories.Abstract
{
    public interface IRequestService
    {
        string ReturnPath(Controller controller);
        bool CheckAjaxRequest(Controller controller);
    }
}
