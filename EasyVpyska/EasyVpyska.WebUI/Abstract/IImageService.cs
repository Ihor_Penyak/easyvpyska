﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using EasyVpyska.Entities;

namespace EasyVpyska.WebUI.Abstract
{
    public interface IImageService
    {
        int UploadPhoto(HttpPostedFileBase photo, int userId, ImageCrop crop);
        ProfileImage LoadPhoto(int userId);
        int UpdatePhoto(HttpPostedFileBase photo, int userId, ImageCrop crop);
    }
}
