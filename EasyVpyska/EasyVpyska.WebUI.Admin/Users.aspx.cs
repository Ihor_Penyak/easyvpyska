﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.SessionState;
using EasyVpyska.Repositories;
using EasyVpyska.Repositories.Sql;

using EasyVpyska.Repositories.Abstract;
using EasyVpyska.WebUI.Admin.Code.Keys;
using EasyVpyska.WebUI.Admin.Code.Models;
using EasyVpyska.WebUI.Admin.Code;
using Ninject;

namespace EasyVpyska.WebUI.Admin
{
    public partial class AdminPage : System.Web.UI.Page
    {
        #region Properties

        private UserFilterModel UserFilter
        {
            get
            {
                Session[SessionKeys.FILTER] = Session[SessionKeys.FILTER] 
                    ?? new UserFilterModel();

                return (UserFilterModel)Session[SessionKeys.FILTER];
            }

            set
            {
                Session[SessionKeys.FILTER] = value;
            }
        }

        #endregion

        #region PageEvents

        protected void Page_Load(object sender, EventArgs e)
        {
            Form.DefaultButton = btnSearch.UniqueID;
            Form.DefaultFocus = tbSearchWord.UniqueID;

            lblDate.Text = DateTime.Now.ToShortDateString().ToString();
            lblTime.Text = DateTime.Now.ToShortTimeString().ToString();

            IUserRepository ur = ServiceLocator.Kernel.Get<IUserRepository>();

          //  UserRepository ur = new UserRepository(c);
            lblUsers.Text = ur.GetUsersCount().ToString();
            Page.Title = "Users - Administrate";
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Status.SelectedIndex = UserFilter.Status ?? 0;
        } 

        #endregion

        #region ControlsEvents

        protected void ODS_GridViewUsers_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = ServiceLocator.Kernel.Get<IUserRepository>();
        }

        protected void GridViewUsers_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int userId = Convert.ToInt32(((Label)GridViewUsers.Rows[e.RowIndex].FindControl("Id")).Text);
            bool userStatus = Convert.ToBoolean(((Label)GridViewUsers.Rows[e.RowIndex].FindControl("Status")).Text);
            odsUsers.UpdateParameters.Add("id", TypeCode.Int32, userId.ToString());
            odsUsers.UpdateParameters.Add("status", TypeCode.Boolean, userStatus.ToString());
            odsUsers.Update();
        }

        protected void GridViewUsers_RowUpdating1(object sender, GridViewUpdateEventArgs e)
        {
            int userId = Convert.ToInt32(((Label)GridViewUsers.Rows[e.RowIndex].FindControl("Id")).Text);
            bool userStatus = Convert.ToBoolean(((Label)GridViewUsers.Rows[e.RowIndex].FindControl("Status")).Text);

            odsUsers.UpdateParameters.Add("Id", TypeCode.Int32, userId.ToString());
            odsUsers.UpdateParameters.Add("Status", TypeCode.Boolean, userStatus.ToString());
            odsUsers.Update();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            UserFilter.Text = tbSearchWord.Text;

            GridViewUsers.DataBind();
        }

        protected void ODS_UsersGrid_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            SetFilterKeyParam(e);
            SetStatusParam(e);
        }

        protected void Status_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserFilter.Status = Status.SelectedIndex;

            GridViewUsers.DataBind();
        }

        #endregion

        #region Methods

        private void SetFilterKeyParam(ObjectDataSourceSelectingEventArgs e)
        {
            tbSearchWord.Text = UserFilter.Text;

            e.InputParameters.Remove("text");
            e.InputParameters.Add("text", tbSearchWord.Text);
        }

        private void SetStatusParam(ObjectDataSourceSelectingEventArgs e)
        {
            Status.SelectedIndex = UserFilter.Status ?? 0;

            e.InputParameters.Remove("status");

            if (Status.SelectedValue == "All")
            {
                e.InputParameters.Add("status", null);
            }
            else if (Status.SelectedValue == "Enabled")
            {
                e.InputParameters.Add("status", 1);
            }
            else if (Status.SelectedValue == "Disabled")
            {
                e.InputParameters.Add("status", 0);
            }
        }

        #endregion

    }
}