﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Comments.aspx.cs" Inherits="EasyVpyska.WebUI.Admin.Comments" MasterPageFile="~/MasterAdminPage.Master" %>


<asp:Content ID="pageHead" ContentPlaceHolderID="headTitle" runat="server">
    <h1>Administrate site comments</h1>
    </asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="filter" runat="server">    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div style="margin-left:20%;">
    <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
    <asp:GridView ID="AdsCommentsGridView" runat="server" AutoGenerateColumns="False" DataSourceID="ODSadsComments" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" DataKeyNames="Id">
        <AlternatingRowStyle BackColor="#CCCCCC" />
        <Columns>
            <asp:CommandField ShowEditButton="True" />
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
            <asp:CheckBoxField DataField="IsEnabled" HeaderText="IsEnabled" SortExpression="IsEnabled" />
            <asp:BoundField DataField="AdId" HeaderText="AdId" ReadOnly="True" SortExpression="AdId" />
            <asp:BoundField DataField="UserId" HeaderText="UserId" ReadOnly="True" SortExpression="UserId" />
            <asp:BoundField DataField="Time" HeaderText="Time" ReadOnly="True" SortExpression="Time" />
            <asp:BoundField DataField="Comment" HeaderText="Comment" ReadOnly="True" SortExpression="Comment" />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
    <asp:ObjectDataSource ID="ODSadsComments" runat="server" OnObjectCreating="ODSadsComments_ObjectCreating" SelectMethod="GetAllAdComments" TypeName="EasyVpyska.Repositories.Sql.AdRepository" UpdateMethod="UpdateAdComment">
        <UpdateParameters>
            <asp:Parameter Name="Id" Type="Int32" />
            <asp:Parameter Name="IsEnabled" Type="Boolean" />
        </UpdateParameters>
    </asp:ObjectDataSource>

        </div>
</asp:Content>
