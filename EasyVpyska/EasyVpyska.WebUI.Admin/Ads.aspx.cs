﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.SessionState;
using EasyVpyska.Repositories;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Admin.Code.Keys;
using EasyVpyska.WebUI.Admin.Code.Models;
using Ninject;

using EasyVpyska.WebUI.Admin.Code;

namespace EasyVpyska.WebUI.Admin
{
    public partial class Ads : System.Web.UI.Page
    {
        #region Properties

        protected AdFilter MyFilter
        {
            get
            {
                Session[SessionKeys.AD_FILTER] = Session[SessionKeys.AD_FILTER] ?? new AdFilter();
                return (AdFilter) Session[SessionKeys.AD_FILTER];
            }
            set
            {
                Session[SessionKeys.AD_FILTER] = value;
            }
        }

        #endregion

        #region PageEvents

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "Ads - Administrate";
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            AuthorIdTB.Text = MyFilter.AuthorId == null ? "" : MyFilter.AuthorId.ToString();
            TownTB.Text = MyFilter.Town == null ? "" : MyFilter.Town;
            tbStartDateTime.Text = MyFilter.Start == null ? "" : MyFilter.Start.ToString();
            tbFinishDateTime.Text = MyFilter.Finish == null ? "" : MyFilter.Finish.ToString();
            switch (MyFilter.IsRequest)
            {
                case false: ddlCheckRequest.SelectedIndex = 0;
                    break;
                case true: ddlCheckRequest.SelectedIndex = 1;
                    break;
                case null: ddlCheckRequest.SelectedIndex = 2;
                    break;
            }
            switch (MyFilter.IsEnabled)
            {
                case false: ddlCheckEnabled.SelectedIndex = 0;
                    break;
                case true: ddlCheckEnabled.SelectedIndex = 1;
                    break;
                case null: ddlCheckEnabled.SelectedIndex = 2;
                    break;
            }
        }


        #endregion

        #region ControlsEvents

        protected void ODSads_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = ServiceLocator.Kernel.Get<IAdRepository>();
        }

        protected void GridView1_RowDeleting1(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void AdsGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

        }

        protected void AdsGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("AdComments.aspx?AdId=" + AdsGridView.SelectedDataKey.Value.ToString());
        }

        protected void ODSCommentsForAd_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            string c = ConfigurationManager.ConnectionStrings["ConnectToSql"].ConnectionString;
            e.ObjectInstance = new AdRepository(c);
        }

        protected void ODSadComments_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            string c = ConfigurationManager.ConnectionStrings["ConnectToSql"].ConnectionString;
            e.ObjectInstance = new AdRepository(c);
        }

        protected void FilterBtn_Click(object sender, EventArgs e)
        {
            MyFilter = new AdFilter();
            switch(ddlCheckRequest.SelectedIndex)
            {
                case 0: MyFilter.IsRequest = false;
                    break;
                case 1: MyFilter.IsRequest = true;
                    break;
                case 2: MyFilter.IsRequest = null;
                    break;
                
            }
            switch(ddlCheckEnabled.SelectedIndex)
            {
                case 0: MyFilter.IsEnabled = false;
                    break;
                case 1: MyFilter.IsEnabled = true;
                    break;
                case 2: MyFilter.IsEnabled = null;
                    break;
            }
            MyFilter.AuthorId = AuthorIdTB.Text == "" ? null : new Nullable<int>(Convert.ToInt32(AuthorIdTB.Text) );
            MyFilter.Town = TownTB.Text == "" ? null : TownTB.Text;
            if (tbStartDateTime.Text != "")
            {
                MyFilter.Start = Convert.ToDateTime(tbStartDateTime.Text);
            }
            if (tbFinishDateTime.Text != "")
            {
                MyFilter.Finish = Convert.ToDateTime(tbFinishDateTime.Text);
            }

            AdsGridView.DataBind();
        }

        protected void ODSads_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters.Remove("filter");
            e.InputParameters.Add("filter", MyFilter);
        }

        protected void AdsGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AdsGridView.PageIndex=e.NewPageIndex;
        }

        protected void AdsGridView_DataBinding(object sender, EventArgs e)
        {

        }

        protected void AdsGridView_DataBound(object sender, EventArgs e)
        {

        }

        protected void ODSads_Load(object sender, EventArgs e)
        {

        }

        #endregion
    }
}