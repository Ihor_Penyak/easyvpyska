﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;

using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.WebUI;

namespace EasyVpyska.WebUI.Admin.Code
{
    public static class ServiceLocator
    {
        public static IKernel Kernel { get; set; }

        static ServiceLocator()
        {
            Kernel = new StandardKernel(new EasyVpyskaConfigModule());
        }
    }
}
