﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace EasyVpyska.WebUI.Admin.Code.Abstract
{
    interface ISecurityManager
    {
        bool Authentication(string login, string password);
        bool IsCurrentUserAuthenticated();
        bool IsUserInRole(string login, string role);
        bool RoleExists(string role);

        string GetUserRole(string login, string passwords);
        void RedirectFromLoginPage(string login, bool createPersistanceCokie);
        void SignOut();
        void RedirectToLoginPage();
    }
}
