﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.Entities
{
    public class User
    {  
        public int Id { get; set; }

        public string Login { get; set; }

        public string Role { get; set; }

        public string FirstName { get; set; }
       
        public string SurName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }     

        public string Country { get; set; }

        public string Region { get; set; }

        public string Town { get; set; }

        public string Address { get; set; }     

        public bool Status { get; set; }

        public string About { get; set; }
    }
}
