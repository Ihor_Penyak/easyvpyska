﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.Entities
{
    public class AdFilter
    {
        public bool? IsRequest { get; set; }

        public bool? IsEnabled { get; set; }

        public bool? IsActive { get; set; }

        public int? AuthorId { get; set; }

        public string Town { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? Finish { get; set; }


    }
}
