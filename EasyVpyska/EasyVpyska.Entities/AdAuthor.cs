﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.Entities
{
    public class AdAuthor
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string SurName { get; set; }

        public int Age { get; set; }

        public string Email { get; set; }

        public string Town { get; set; }

        public bool Status { get; set; }

        public string ImageString { get; set; }
    }
}
