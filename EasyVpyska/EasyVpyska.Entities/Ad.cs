﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.Entities
{
    public class Ad
    {
        public int Id { get; set; }

        public int AuthorId
        {
            get
            {
                return Author.Id;
            }
        }

        public bool IsRequest { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsActive { get; set; }

        public string Title { get; set; }

        public AdAuthor Author { get; set; }

        public string Description { get; set; }

        public DateTime BeginTime { get; set; }

        public DateTime EndTime { get; set; }

        public string Country { get; set; }

        public string Town { get; set; }

        public string Address { get; set; }

        public List<AdComment> Comments { get; set; }

        public List<AdSubscribers> Subscribers { get; set; }

        public int Views { get; set; }

        public bool IsActual(DateTime CurrentDateTime)
        {
            bool hasStarted = BeginTime <= CurrentDateTime;
            bool hasNotEnded = EndTime >= CurrentDateTime;
            return hasStarted && hasNotEnded;
        }
    }
}
