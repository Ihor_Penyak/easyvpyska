﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.Entities
{
    public class ProfileImage
    {
        public string FileName { get; set; }
        public byte[] File { get; set; }
        public int UserId { get; set; }
    }
}
