﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.Entities
{
    public class AdComment
    {
        public int Id { get; set; }

        public bool IsEnabled { get; set; }

        public int AdId { get; set; }

        public int? UserId { get; set; }

        public string UserName { get; set; }

        public DateTime Time { get; set; }

        public string Comment { get; set; }
    }
}
