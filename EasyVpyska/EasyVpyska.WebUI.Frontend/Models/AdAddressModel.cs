﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyVpyska.WebUI.Frontend.Models
{
    public class AdAddressModel
    {
        public string Address { get; set; }
        public int Id { get; set; }
    }
}