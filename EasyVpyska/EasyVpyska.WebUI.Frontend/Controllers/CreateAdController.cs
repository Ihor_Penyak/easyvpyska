﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

using EasyVpyska.Entities;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;
using EasyVpyska.WebUI.Abstract;
using System.Net.Mail;
using System.Net;
using System.Threading.Tasks;

namespace EasyVpyska.WebUI.Frontend.Controllers
{

    public class CreateAdController : Controller
    {
        #region Fields
        private readonly IAdRepository _adRepository;
        private readonly ISecurityManager _securityManager;
        private readonly IUserRepository _userRepository;
        private readonly IEmailService _emailService;
        #endregion

        #region Constructor
        public CreateAdController(IUserRepository userRepository, ISecurityManager securityManager, IAdRepository adRepository, IEmailService emailService)
        {
            _adRepository = adRepository;
            _securityManager = securityManager;
            _userRepository = userRepository;
            _emailService = emailService;
        }
        #endregion

        #region Methods
        //
        // GET: /CreateAd/
        [HttpGet]
        public ActionResult Index()
        {

            bool login = _securityManager.IsCurrentUserAuthenticated();
            ViewBag.UserLogin = login;
            if (login)
            {
                ViewBag.LoginUserId = _securityManager.GetCurrentUserId();
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(string adJson)
        {
            int adId = -1;
            JavaScriptSerializer _jsonSerializer = new JavaScriptSerializer();
            var ad = _jsonSerializer.Deserialize<Ad>(adJson);
            var violations = new Dictionary<string, string>();

            CheckNewAdRules(ad, violations);

            if (violations.Count == 0)
            {
                adId = _adRepository.CreateAd(ad);
            }

            var data = new
            {
                adId = adId,
                violations = violations
            };

            var serializator =
                new System.Web.Script.Serialization.JavaScriptSerializer();

            string dataJson = serializator.Serialize(data);

            return Content(dataJson, "application/json");
        }

        [HttpPost]
        public ActionResult CreateWithRegister(string adJson, string userJson)
        {
            int adId = -1;

            string password = this.CreateRandomPassword();
            var violations = new Dictionary<string, string>();

            JavaScriptSerializer _jsonSerializer = new JavaScriptSerializer();
            var ad = _jsonSerializer.Deserialize<Ad>(adJson);
            var user = _jsonSerializer.Deserialize<User>(userJson);

            AdAuthor author = new AdAuthor();

            ad.Author = author;
            
            user.Role = "user";
            user.Status = true;

            int register = -1;

            CheckNewUserRules(user, violations);
            CheckNewAdRules(ad, violations);

            if (violations.Count == 0)
            {
                register = _securityManager.RegistrateUser(user, password);

                if (register != -1)
                {
                    Task emailTask = Task.Run(() =>
                    {
                        try
                        {
                            string mailSubject = "Congratulation! You have been registrated on our site!";
                            string mailBody = "Your login: " + user.Login + "<br /><br />"
                                   + "Your password: " + password + "<br /><br />"
                                   + "Please, visit our <a href='easyvpyska.somee.com'>site</a> and edit your password and profile." + "<br /><br />"
                                   + "Best regards, EasyVpyska";
                            _emailService.SendEmail(user.Email, mailSubject, mailBody);
                        }
                        catch (Exception e)
                        {
                            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                            logger.Error(e.Message);

                        }
                    });

                    if (ad.Author.Id == 0)
                    {
                        ad.Author.Id = _userRepository.GetUserId(user.Login);
                    }

                    adId = _adRepository.CreateAd(ad);
                }
            }

            var data = new
            {
                adId = adId,
                violations = violations
            };

            var serializator =
                new System.Web.Script.Serialization.JavaScriptSerializer();

            string dataJson = serializator.Serialize(data);


            return Content(dataJson, "application/json");

        }

        [HttpPost]
        public JsonResult IsEmailAwaliable(string txtEmail)
        {
            #region Validation
            if (txtEmail == null)
            {
                throw new ArgumentNullException();
            }
            #endregion

            if (_userRepository.IsEmailExist(txtEmail))
            {
                return new JsonResult { Data = false };
            }

            return new JsonResult { Data = true };
        }

        #endregion

        #region Helpers

        private string CreateRandomPassword()  
        {
            string _allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789-";
            Random randNum = new Random((int)DateTime.Now.Ticks);  //Don't forget to seed your random, or else it won't really be random
            char[] chars = new char[8];
            
            for (int i = 0; i < 8; i++)
            {
                chars[i] = _allowedChars[randNum.Next(_allowedChars.Length)];                
            }
            return new string(chars);
        }
        private void CheckNewUserRules(User user,
            IDictionary<string, string> violations)
        {
            CheckWordRules(user.FirstName, "txtName", violations);

            CheckEmailRules(user.Login, "txtEmail", violations);
            CheckEmailRules(user.Email, "txtEmail", violations);

            CheckBirthDateRules(user.DateOfBirth, "txtBirthDate", violations);
        }

        private void CheckNewAdRules(Ad ad,
            IDictionary<string, string> violations)
        {
            CheckAddressRules(ad, "txtAddress", violations);

            CheckFutureDateRules(ad.BeginTime, "txtArrivalDate", violations);
            CheckFutureDateRules(ad.EndTime, "txtLeavingDate", violations);

            CheckSettlementDatesRules(ad.BeginTime, ad.EndTime, "txtLeavingDate", violations);

            CheckTextLenth(ad.Address, "txtAddress", violations, 64);
            CheckTextLenth(ad.Country, "txtAddress", violations, 64);
            CheckTextLenth(ad.Town, "txtAddress", violations, 64);
            CheckTextLenth(ad.Title, "txtTitle", violations, 25);
            CheckTextLenth(ad.Description, "txtDescription", violations, 256);
        }


        private void CheckAddressRules(Ad ad, string fieldName,
            IDictionary<string, string> violations)
        {
            var regex = new Regex("^[a-zA-Z]+$");

            if (string.IsNullOrEmpty(ad.Country)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter country name please."));
            }

            if (string.IsNullOrEmpty(ad.Town)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter town name please."));
            }
        }

        private void CheckWordRules(string word, string fieldName,
           IDictionary<string, string> violations,
           int minLength = 3,
           int maxLength = 64)
        {
            var regex = new Regex("^[a-zA-Z]+$");

            if (string.IsNullOrEmpty(word)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (word != null
                && word.Length < minLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter at least " + minLength.ToString() +
                    " 3 characters."));
            }
            if (word != null
                && word.Length > maxLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter no more than " + maxLength.ToString() +
                    " characters."));
            }
            if (word != null
                && !regex.IsMatch(word)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please use just english letters."));
            }
        }

        private void CheckEmailRules(string email, string fieldName,
            IDictionary<string, string> violations)
        {
            var regex = new Regex("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+"
                + "(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

            if (string.IsNullOrEmpty(email)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (email != null
                && !regex.IsMatch(email)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter a valid email address."));
            }
            if (email != null
                && _userRepository.IsEmailExist(email)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    " This email is already taken."));
            }
        }


        private void CheckTextLenth(string text, string fieldName,
          IDictionary<string, string> violations,
          int maxLength)
        {
            if (text != null
                && text.Length >= maxLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                     "Please enter no more than " + maxLength.ToString() +
                    " characters."));
            }
        }

        private void CheckBirthDateRules(DateTime dateOfBirth, string fieldName,
           IDictionary<string, string> violations
           )
        {
            if (dateOfBirth == null
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (dateOfBirth != null
                && GetAge(dateOfBirth) >= 130
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter correct date. You should be younger"));
            }
            if (dateOfBirth != null
                && GetAge(dateOfBirth) < 5
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter correct date. You should be older"));
            }
        }

        private void CheckFutureDateRules(DateTime date, string fieldName,
           IDictionary<string, string> violations)
        {
            if (date == null
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (date != null
                && !IsFutureDate(date)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter correct date. It should be greater than today"));
            }
        }

        private void CheckSettlementDatesRules(DateTime arrivalDate, DateTime livingDate, string fieldName,
           IDictionary<string, string> violations
           )
        {
            if (arrivalDate != null
                && livingDate != null
                && livingDate < arrivalDate
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter correct date. Living Date should be grater than arrival"));
            }
        }

        private int GetAge(DateTime dateOfBirth)
        {
            return (DateTime.Today.Year - dateOfBirth.Year);
        }

        private bool IsFutureDate(DateTime date)
        {
            return (DateTime.Today <= date);
        }

        #endregion
    }
}