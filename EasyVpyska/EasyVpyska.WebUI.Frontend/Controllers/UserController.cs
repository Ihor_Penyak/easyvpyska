﻿using EasyVpyska.Entities;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.WebUI.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyVpyska.WebUI.Frontend.Controllers
{
    public class UserController : Controller
    {
         #region Fields
         private readonly IUserRepository _userRepository;
         private readonly IAdRepository _adRepository;
         private readonly ISecurityManager _securityManager;
        #endregion

         #region Constructor
         public UserController(IUserRepository userRepository, ISecurityManager securityManager, IAdRepository adRepository)
        {
            _userRepository = userRepository;
            _securityManager = securityManager;
            _adRepository = adRepository;
        }
         #endregion

         #region Methods
         // GET: User
       // [Authorize(Roles = "admin, user")]
         [HttpGet]
         [Route("User/{id?}")]
        public ActionResult Index(string id)
        {
            int currentId;

            if (!int.TryParse(id, out currentId))
            {
                throw new ArgumentException("User Id should be an integer varialbe");
            }

            var User = _userRepository.GetUserById(currentId);
            var image = _userRepository.LoadPhoto(currentId);

            if (User != null)
            {
                ViewBag.Profile = User;

                if (image.File != null)
                { 
                    var base64Image = "data:image/jpg;base64," + Convert.ToBase64String(image.File);
                    ViewBag.Image = base64Image;
                }
                else
                {
                    ViewBag.Image = @"/Styles/Image/icon-user.png";
                }
            }

            AdFilter filter = new AdFilter();
            filter.AuthorId = currentId;

            int countAd = _adRepository.CountSuitableAds(filter);
            filter.IsEnabled = true;

            ViewBag.EnableCount = _adRepository.CountSuitableAds(filter);
      
            bool userLog = _securityManager.IsCurrentUserAuthenticated();

            ViewBag.AdsCount = countAd;
            ViewBag.UserLog = userLog;
            if (userLog)
            {
                ViewBag.loginUsersId = _securityManager.GetCurrentUserId();
            }
             
            DateTime todayDate = DateTime.Today;
            TimeSpan timeSpan = (TimeSpan)(todayDate - User.DateOfBirth);
            ViewBag.Age = (int)(timeSpan.TotalDays / 365.0);

            return View("Index");
        }

         [HttpPost]
         public PartialViewResult AllAds(int Id, int ads)
         {
    
             bool userLog = _securityManager.IsCurrentUserAuthenticated();
             ViewBag.LoginUser= userLog;
             if (userLog)
             {
                 ViewBag.idLoginUser = _securityManager.GetCurrentUserId();
             }

             AdFilter filter = new AdFilter();
             filter.AuthorId = Id;
             filter.IsEnabled = true;
             if (_securityManager.GetCurrentUserId() != Id)
             {                
                 filter.IsActive = true;
                 filter.Start = DateTime.Now;
             }

             List<Ad> adsFilterList = _adRepository.GetAds(filter, 1, ads);

             ViewBag.Count = _adRepository.CountSuitableAds(filter);   
             
             ViewBag.Ads = ads;
             ViewBag.ListOfAds = adsFilterList;
         
             return PartialView("PartialAdsInUserProfile");
         }

        [HttpPost]
         public PartialViewResult SetActiveAd(int Id, bool status)
        {
            _adRepository.UpdateAdActiveStatus(Id, status);

            return PartialView("PartialAdsInUserProfile");
        }

        [HttpPost]
        public PartialViewResult DeleteAd(int Id)
        {
            _adRepository.DeleteAd(Id);

            return PartialView("PartialAdsInUserProfile");
        }
        
        public PartialViewResult SearchSuitableAds(int Id)
        {
            Ad currentAd = _adRepository.GetAd(Id);

            AdFilter filter = new AdFilter();

            filter.IsEnabled = true;
            filter.IsRequest = !currentAd.IsRequest;
            filter.IsActive = true;
            filter.Town = currentAd.Town;
            filter.Start = currentAd.BeginTime;
            filter.Finish = currentAd.EndTime;

            List<Ad> suitableAds = new List<Ad>();

            //find 5 suitable ads
            suitableAds = _adRepository.GetAds(filter, 1, 5);

            ViewBag.SuitableAds = suitableAds;

            return PartialView("PartialSuitableAds");
        }
         #endregion
    }
}