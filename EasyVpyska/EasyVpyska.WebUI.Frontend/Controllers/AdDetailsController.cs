﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using EasyVpyska.Entities;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;
using System.Web.SessionState;

using EasyVpyska.WebUI.Frontend.Models;
using EasyVpyska.WebUI.Abstract;
using System.Threading.Tasks;

namespace EasyVpyska.WebUI.Frontend.Controllers
{
    
    public class AdDetailsController : Controller
    {
        #region Fields
        private readonly IAdRepository _adRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISecurityManager _securityManager;
        private readonly IEmailService _emailService;
        private readonly IImageService _imageService;
        #endregion

        #region Constructor
        public AdDetailsController(IAdRepository adRepository, ISecurityManager securityManager, 
            IEmailService emailService, IUserRepository userRepository,
            IImageService imageService)
        {
            _adRepository = adRepository;
            _securityManager = securityManager;
            _emailService = emailService;
            _userRepository = userRepository;
            _imageService = imageService;
        }
        #endregion

        #region Methods
        // GET: /AdDetails/
        [HttpGet]
        [Route("AdDetails/{id?}")]
        public ActionResult Index(string id)
        {
            int currentId;
           
       
            if (!int.TryParse(id, out currentId))
            {
                throw new ArgumentException("Should be an integer varialbe");
            }

            var Ad = _adRepository.GetAd(currentId);
            if(Ad!=null)
            {
                if (_securityManager.IsCurrentUserAuthenticated())
                {
                    int userId = _securityManager.GetCurrentUserId();
                    ViewBag.CurrentUserId = userId;
                    _adRepository.InsertViews(userId, currentId);

                    bool isSubscribe = SubscribersCheck(Ad, userId);
                    ViewBag.IsSubscribe = isSubscribe;                  
                }                
                ViewBag.Ad = Ad;
            }

            var image = _imageService.LoadPhoto(Ad.Author.Id);

            if (image.File != null)
            {
                var base64Image = "data:image/jpg;base64," + Convert.ToBase64String(image.File);
                ViewBag.Image = base64Image;
            }
            else
            {
                ViewBag.Image = @"/Styles/Image/icon-user.png";
            }
                                   
            return View();
        }

        [HttpPost]
        public ActionResult GetAddresses(int Id)
        {
            List<AdAddressModel> addresses = new List<AdAddressModel>();

            var ad = _adRepository.GetAd(Id);

            addresses.Add(new AdAddressModel() { Address = ad.Town + " " + ad.Address, Id = ad.Id });

            return Json(new
            {
                startPosition = new { latitude = 49.6594482, longitude = 31.5560814, zoom = 6 },
                addresses
            });
        }

        [HttpPost]
        public ActionResult GetAdInfo(int Id)
        {
            var Ad = _adRepository.GetAd(Id);          

            string name = "";
            string description = "";
            string userLink = "";

            if (Ad != null)
            {
                description = Ad.Description;
            }

            if(Ad.Author != null)
            {
                name = Ad.Author.FirstName;              
                userLink = Ad.Author.Id.ToString();
            }
            
            return Json(new
            {
                name = name,    
                userLink = userLink,
                description = description
            });
        }

        [HttpPost]
        public ActionResult UserSubscribed(int adId)
        {
            int userId=_securityManager.GetCurrentUserId();                      
           
            bool result = _adRepository.SubscribeOnAd(userId,adId);

            Ad ad = _adRepository.GetAd(adId);

            string subject = "You have a new subscriber";
            string body = "Somebody subscribes on your ad " + '"' + ad.Title + '"' +
                        ".<br/>Visit our <a href='easyvpyska.somee.com/User/Index/" + ad.Author.Id + "'>site</a> to view it." + "<br/>Best regards, EasyVpyska";

            _emailService.SendEmail(ad.Author.Email, subject, body);

            return Json(new
            {
                result
            });
        }
        
        [HttpPost]
        public ActionResult AddComment(int adId, string comment)
        {            
            IEnumerable<string> validationMessages = this.CheckNewCommetRules(comment);
            if (validationMessages.Count() > 0)
            {
                return Json(new
                {
                    failed = true,
                    validation = validationMessages
                });
            }
            
            DateTime now = DateTime.Now;
            int? userId = null;
            string commentAuthorName = "Guest";

            if (_securityManager.IsCurrentUserAuthenticated())
            {
                userId = _securityManager.GetCurrentUserId();           
                commentAuthorName = _userRepository.GetUserById((int)userId).FirstName;
            }           

            AdComment adComment = new AdComment()
            {
                AdId = adId,
                Comment = comment,
                Time = now,
                UserId = userId
            };
            int commentId = _adRepository.AddComment(adComment);

            Ad ad = _adRepository.GetAd(adId);
            string authorEmail = ad.Author.Email;

            Task emailTask = Task.Run(() =>
                {
                    try
                    {
                        string subject = "You have a new comment to your ad";
                        string body = "User " + commentAuthorName + " leave the comment for your ad " + '"' + ad.Title + '"' +
                                    ".<br/>Visit our <a href='easyvpyska.somee.com/AdDetails/Index/" + ad.Id + "'>site</a> to read it." + "<br/>Best regards, EasyVpyska";
                        _emailService.SendEmail(authorEmail, subject, body);
                     //   throw new Exception("MAIL ERROR");
                    }
                    catch (Exception e)
                    {                       
                        NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                        logger.Error(e.Message);
                    }
                });

            return Json(new
                {
                    adComment = adComment,                    
                    userName = commentAuthorName
                });
        }

        #endregion

        #region Helpers

        private IEnumerable<string> CheckNewCommetRules(string comment)
        {
            List<string> violations = new List<string>();
            if (string.IsNullOrEmpty(comment))
            {
                violations.Add("Comment should not be empty");
            }
            if (comment != null && comment.Length > 500)
            {
                violations.Add("Comment length should not exceed 500 symbols");
            }
            return violations;
        }

        private bool SubscribersCheck(Ad ad, int UserId)
        {
            bool result = false;
            if (ad.Subscribers.Count > 0)
            {
                foreach (var a in ad.Subscribers)
                {
                    if (a.UserId == UserId)
                    {
                        result = true;
                    }

                }
            }
            return result;        
        }
        #endregion
    }
}