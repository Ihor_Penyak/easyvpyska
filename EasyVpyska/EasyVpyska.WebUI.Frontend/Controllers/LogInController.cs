﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using EasyVpyska.WebUI;
using EasyVpyska.WebUI.Abstract;
using System.Net.Mail;
using System.Net;

namespace EasyVpyska.WebUI.Frontend.Controllers
{
    public class LogInController : Controller
    {
        #region Fields
        private readonly ISecurityManager _securityManager;
        private readonly IEmailService _emailService;
        #endregion

        #region Constructor
        public LogInController(ISecurityManager securityManager, IEmailService emailService)
        {
            _securityManager = securityManager;
            _emailService = emailService;
        }
        #endregion

        #region Methods

        [HttpPost]
        public ActionResult LogIn(string name, string password)
        {
            int userId = -1;
            bool blocked = false;
            bool authenticated = false;

            IDictionary<string, string> violations = new Dictionary<string, string>();

            if (_securityManager.Authentication(name, password))
            {
                if (!_securityManager.IsUserEnabled(name))
                {
                    blocked = true;

                    violations.Add(new KeyValuePair<string, string>("password",
                        "User is blocked"));
                }
                else
                {
                    _securityManager.SetAuthCookie(name, true);

                    userId = _securityManager.GetUserId(name);

                    authenticated = true;
                }
            }
            else
            {
                violations.Add(new KeyValuePair<string, string>("password",
                    "Incorrect Login/Password combination"));
            }



            var data = new
            {
                blocked = blocked,
                authenticated = authenticated,
                userId = userId,
                violations = violations
            };

            var serializator =
                new System.Web.Script.Serialization.JavaScriptSerializer();

            string dataJson = serializator.Serialize(data);

            return Content(dataJson, "application/json");
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            if (_securityManager.IsCurrentUserAuthenticated())
            {
                _securityManager.SignOut();

                return Json(new { logOutSuccess = true });
            }

            return Json(new { logOutSuccess = false });
        }

        [HttpGet]
        public ActionResult IsCurrentUserSigned()
        {
            if (_securityManager.IsCurrentUserAuthenticated())
            {
                return Json(new { authenticated = true });
            }

            return Json(new { authenticated = false });
        }

        [HttpPost]
        public ActionResult SendMailToAdmin(string message)
        {
            try
            {
                _emailService.SendEmail("easyvpyska@gmail.com", "Feedback from user", "Text:<br /><br />" + message);
                return Json(new { result = true });
            }
            catch (Exception e)
            {
                NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Error(e.Message);
                return Json(new { result = true });
            }
        }
        #endregion
    }
}