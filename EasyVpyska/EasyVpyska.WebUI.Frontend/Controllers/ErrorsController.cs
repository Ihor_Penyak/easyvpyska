﻿using EasyVpyska.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyVpyska.WebUI.Frontend.Controllers
{
    public class ErrorsController : Controller
    {
         #region Fields
          private readonly IRequestService _requestService;
         #endregion 

         #region Constructor
          public ErrorsController(IRequestService requestService)
        {
            _requestService = requestService;         
        }
         #endregion

         #region Methods
        //
        // GET: /Errors/
          public ActionResult Error404()
        {
            ActionResult result;
            object model = _requestService.ReturnPath(this);

              if (!_requestService.CheckAjaxRequest(this))
                result = View("Error404");
            else
                result = View("Error404", model);

            return result;
        }

          public ActionResult Error500()
          {
              ActionResult result;
              object model = _requestService.ReturnPath(this);

              if (!_requestService.CheckAjaxRequest(this))
              {
                  result = View("Error500");
              }
              else
              {
                  result = View("Error500", model);
              }

              return result;
          }
         #endregion
    }
}