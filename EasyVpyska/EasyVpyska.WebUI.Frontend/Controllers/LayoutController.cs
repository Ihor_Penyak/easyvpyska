﻿using EasyVpyska.WebUI.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyVpyska.WebUI.Frontend.Controllers
{
    public class LayoutController : Controller
    {
        #region Fields
        private readonly ISecurityManager _securityManager;
        #endregion
        
        #region Constructor
        public LayoutController(ISecurityManager securityManager)
        {
            _securityManager = securityManager;
        }
        #endregion

        #region Methods
        [ChildActionOnly]
       public ActionResult PartialAuthenticated()
        {
            bool userLog = _securityManager.IsCurrentUserAuthenticated();
            ViewBag.UserLog = userLog;
            if (userLog)
            {
                ViewBag.loginUsersId = _securityManager.GetCurrentUserId();
            }

            return PartialView("PartialAuthenticated");
        }

        [ChildActionOnly]
        public ActionResult PartialLogOut()
        {
            bool userLog = _securityManager.IsCurrentUserAuthenticated();
            ViewBag.UserLog = userLog;
    
            return PartialView("PartialLogOut");
        }
        #endregion
    }
}