﻿$(function () {
    $('#dialog-modal').dialog({
        autoOpen: false,
        modal: true,
        width: 460,
        height: 435
    });

    $('#dialog-feedback').dialog({
        autoOpen: false,
        modal: true,
        width: 600,
        height: 310
    });

    $('#dialog-about').dialog({
        autoOpen: false,
        modal: true,
        width: 500,
        height: 260
    });

    console.log('add dialog');

    $('#about').click(function () {
        $('#dialog-about').dialog("open");
        $('#close-button').click();
    });

    $('#feedback').click(function () {
        $('#dialog-feedback').dialog("open");
        $('#close-button').click();
    });

    $('#logIn').click(function () {
        $('#dialog-modal').dialog("open");
        $('#close-button').click();
    });
});

