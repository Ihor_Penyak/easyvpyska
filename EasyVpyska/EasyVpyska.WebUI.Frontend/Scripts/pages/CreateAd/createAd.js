﻿

(function ($) {

    var $validator = $("#myform").validate({
        rules:
            {
                txtTitle: {
                    required: true,
                    maxlength: 25,
                    minlength: 3
                },
                txtName: {
                    required: true,
                    maxlength: 16,
                    minlength: 3,
                    regex: /^[a-zA-Z]+$/,
                },
                txtArrivalDate: {
                    required: true,
                    date: true,
                },
                txtLeavingDate: {
                    required: true,
                    date: true,
                },
                txtBirthDate: {
                    required: true,
                    date: true,
                },
                txtEmail: {
                    required: true,
                    email: true,
                    remote: {
                        url: "/CreateAd/IsEmailAwaliable",
                        timeout: 2000,
                        type: "post"
                    }
                },
                txtAddress: {
                    required: true,
                    maxlength: 128
                },
                txtDescription: {
                    maxlength: 256
                }
            },
        messages:
        {
            txtEmail: {
                remote: "This email is already taken."
            },
            txtTitle: {
                regex: "Please use english letters or digits only"
            },
            txtName: {
                regex: "Please use english letters only"
            }
        }
    });


    var CreateAdPage = function () {

        var that = this;
        var Autocomplete;

        var componentForm = {
            street_number: { type: 'short_name', field: '' },
            route: { type: 'long_name', field: '' },
            locality: { type: 'long_name', field: '' },
            country: { type: 'long_name', field: '' },
        };


        this.initialize = function () {

            console.log("[create ad page-page] initialize", arguments);

            this.$createButton = $("#btnCreateAd");

            //this.$txtName = $("#txtName");
            this.$txtTitle = $("#txtTitle");
            this.$txtDescription = $("#txtDescription");
            this.$txtArrivalDate = $("#txtArrivalDate");
            this.$txtLeavingDate = $("#txtLeavingDate");
            this.$txtAddress = $("#txtAddress");
            this.$txtTitle = $("#txtTitle");
            this.$AuthorId = $("#currentUser");
            this.$rbIsRequest = $("#q3c");
            this.today = new Date();

            this.$createButton.on("click", this.onCreate);

        };

        this.autocompleteInitialize = function () {

            var addressInput = document.getElementById('txtAddress');

            addressAutocomplete = new google.maps.places.Autocomplete(addressInput);

            addressAutocomplete.addListener("place_changed", this.fillInAddress);
        }

        this.fillInAddress = function () {
            that.fillIt(this);
        };

        this.fillIt = function (autocomplete) {
            var place = autocomplete.getPlace();

            for (addressInputType in componentForm) {
                componentForm[addressInputType].field = "";
            }

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];

                for (addressInputType in componentForm) {
                    if (addressType === addressInputType) {
                        var val = place.address_components[i][componentForm[addressType].type];
                        componentForm[addressType].field = val;
                    }
                }
            }
        }

        this.onCreate = function () {
            console.log("[create ad page-page] onCreate", arguments, this);
            console.log(that.$AuthorId.val());

            var valid = $("#myform").valid();

            if (valid){

                if (that.$AuthorId.val() != 0) {
                    var Ad = Object();
                    Ad.Author = {
                        Id: that.$AuthorId.val()    
                    }
                    //get(0)
                    Ad.IsRequest = that.$rbIsRequest[0].checked;

                    Ad.Title = that.$txtTitle.val();
                    Ad.Description = that.$txtDescription.val();
                    Ad.Address = componentForm['route'].field;
                    Ad.Address += ' ' + componentForm['street_number'].field;
                    Ad.BeginTime = that.$txtArrivalDate.val();
                    Ad.EndTime = that.$txtLeavingDate.val();
                    Ad.Country = componentForm['country'].field;
                    Ad.Town = componentForm['locality'].field;
                    Ad.Title = that.$txtTitle.val();

                    $.ajax({
                        url: "/CreateAd/Create",
                        type: "POST",
                        dataType: "json",
                        data: {
                            adJson: JSON.stringify(Ad),
                        }
                    }).done(function (data) {
                        console.log("[fraction-page] onCreate - done", arguments, this);

                        if (data.adId != -1) {
                            window.location.replace("/AdDetails/index/" + data.adId);
                        }
                        else {
                            $validator.showErrors(data.violations);
                        }
                        
                    });
                }
                else {
                    var NewUser = Object();

                    var login = $("#txtEmail").val();
                
                    NewUser.Login = login;
                    NewUser.FirstName = $("#txtName").val();
                    NewUser.DateOfBirth = $("#txtBirthDate").val();
                    NewUser.Email = $("#txtEmail").val();            
     
                    var Ad = Object();
                               
                    Ad.Author = {
                        Id: 0
                    }
                    Ad.IsRequest = that.$rbIsRequest[0].checked;                
                    Ad.Title = that.$txtTitle.val();
                    Ad.Description = that.$txtDescription.val();
                    Ad.Address = componentForm['route'].field;
                    Ad.Address += ' ' + componentForm['street_number'].field;
                    Ad.BeginTime = that.$txtArrivalDate.val();
                    Ad.EndTime = that.$txtLeavingDate.val();
                    Ad.Country = componentForm['country'].field;
                    Ad.Town = componentForm['locality'].field;
                    Ad.Title = that.$txtTitle.val();

                    
                    $.ajax({
                        url: "/CreateAd/CreateWithRegister",
                        type: "POST",
                        dataType: "json",
                        data: {
                            adJson: JSON.stringify(Ad),
                            userJson: JSON.stringify(NewUser)
                        }
                    }).done(function (data) {
                        console.log("[fraction-page] onCreateRegister - done", arguments, this);

                        if (data.adId != -1) {
                            window.location.replace("/AdDetails/index/" + data.adId);
                        }
                        else {
                            $validator.showErrors(data.violations);
                        }

                    });
                }
            }
        };
    };

  

    $(function () {
        console.log("CreateAdPage page has loaded");
        var page = new CreateAdPage();
        page.initialize();
        page.autocompleteInitialize();
              
        $("#txtBirthDate").datepicker({
            showOn: "focus",           
            dateFormat: "mm.dd.yy",
            constrainInput: true,
            maxDate: page.today,
            yearRange: "c-25:+00",
            changeYear: true
        });

        $("#txtArrivalDate").datepicker({
            showOn: "focus",         
            dateFormat: "mm.dd.yy",
            constrainInput: true,
            minDate: page.today
        });

        $("#txtLeavingDate").datepicker({
            showOn: "focus",     
            dateFormat: "mm.dd.yy",
            constrainInput: true,
            minDate: page.today
           
        });
    });

})(window.jQuery);
