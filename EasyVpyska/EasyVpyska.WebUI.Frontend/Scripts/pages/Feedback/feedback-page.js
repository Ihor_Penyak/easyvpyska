﻿(function ($) {

    var Feed = function () {

        var that = this;

        this.initialize = function () {
            console.log("[Feed-partitial view] initialize", arguments);

            this.$txtMessage = $("#message");   

            this.$send = $("#sendMail");            

            this.$send.on("click", this.onSendMail);
          
        };

        this.onSendMail = function () {

            console.log("[Feed-partitial view] onSendMail", arguments, this);

            var message = that.$txtMessage.val();            

            $.ajax({
                url: "/LogIn/SendMailToAdmin",
                type: "POST",
                dataType: "json",
                data: {
                    message: message                   
                }
            }).done(function (data) {
                console.log("[Feed-partitial view] onSendMail - done", arguments, this);
                console.log(data.result);
                if (data.result) {
                    $('.writeMessage').hide();
                    $('.thnks').show();
                }
                // console.log("[logIn-partitial view] onSingIn - non authenticated", arguments, this);
            });
        };
             
    };

    $(document).on("ready", function () {
        console.log("[Feed-partitial] has loaded", arguments, this);
        var page = new Feed();
        page.initialize();
    });

})(window.jQuery);