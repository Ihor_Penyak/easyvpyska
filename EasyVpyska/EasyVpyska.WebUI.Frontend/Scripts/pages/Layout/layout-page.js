﻿(function ($) {

    var LayoutPage = function () {

        var that = this;

        this.today = new Date();

        this.initialize = function () {
            console.log("[_layout page] initialize", arguments);

            this.$logOut = $("#logOut");

            this.$logOut.on("click", this.onLogOut);
        };

        this.onLogOut = function () {
            console.log("[_layout page] onLogOut", arguments, this);
            $.ajax({
                url: "/LogIn/LogOut",
                type: "POST",
                dataType: "json",
            }).done(function (data) {
                console.log("[_layout page] onLogOut - done", arguments, this);

                if (data.logOutSuccess) {
                    console.log("[_layout page] onLogOut - logOut success", arguments, this);
                    location.reload(true);
                } else {
                    console.log("[_layout page] onLogOut - logOut unsuccess", arguments, this);
                }

            });
        };
        
    };
    $(function () {
        console.log("Home page has loaded");
        var page = new LayoutPage();
        page.initialize();

        $(".center #date-of-birth").datepicker({
            dateFormat: "mm.dd.yy",
            constrainInput: true,
            maxDate: page.today,      
            yearRange: "c-25:+00",
            changeYear: true
        });
        
        $(".container .menu-button").tooltip({
            content: "Open Menu",
            show: { effect: "fade", duration: 500 }
        });
    });
})(window.jQuery);


