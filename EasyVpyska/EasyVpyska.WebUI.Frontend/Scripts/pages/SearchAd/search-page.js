﻿

(function ($) {

    var Filter = function () {

        var that = this;

        this.initialize = function () {
            console.log("[filter-page] initialize", arguments);

            this.$typeAd = $("#typeAd");
            this.$city = $("#city");
            this.$arrdate = $("#ArrivalDate");
            this.$leavdate = $("#leaveDate");
            this.$count = $("#count");
            this.$adsOnPage = $("#changeNumbersUp");
            this.$adsOnPageD = $("#changeNumbersDown");

            this.$searchbtn = $("#search-button");           
            this.$firstPage=$("#first");
            this.$previousPage = $("#previous");
            this.$currentPage=$("#currentpageUp");
            this.$nextPage = $("#next");
            this.$lastPage = $("#last");
            this.today = new Date();
        
            console.log(this.$currentPage, this.$adsOnPage);

            this.$searchbtn.on("click", this.onFilterUp);
            this.$adsOnPage.on ("change", this.onFilterUp);
            this.$adsOnPageD.on("change", this.onFilterDown);
        };

        this.onPageFilter = function (currentPage) {
            console.log("[filter-page] onFilter", arguments, this);

            var typeAd = that.$typeAd.val();
            var city = that.$city.val();
            var ArrivalDate = that.$arrdate.val();
            var leaveDate = that.$leavdate.val();
            var page = currentPage;
            document.getElementById("currentpageUp").value = currentPage;
            document.getElementById("currentpageDown").value = currentPage;

            var ads = that.$adsOnPage.val();

            $.ajax({
                url: "/SearchAd/Filter",
                cache: false,
                type: "POST",
                dataType: "html",
                data: {
                    typeAd: typeAd,
                    city: city,
                    page: page,
                    ads: ads,
                    ArrivalDate: ArrivalDate,
                    leaveDate: leaveDate
                }
            }).done(function (partialViewResult) {
                console.log("[filter-page] onFilter - done", arguments, this);
                //console.log(partialViewResult);
                document.getElementById("changeNumbersDown").value = ads;
                $('#results').html(partialViewResult);
                
            });
        };
        this.onFilterUp = function () {
            console.log("[filter-page] onFilter", arguments, this);

            var typeAd = that.$typeAd.val();
            var city = that.$city.val();
            var ArrivalDate = that.$arrdate.val();
            var leaveDate = that.$leavdate.val();
            var page = 1;
            document.getElementById("currentpageUp").value = 1;
            document.getElementById("currentpageDown").value = 1;
            var ads = that.$adsOnPage.val();

            $.ajax({
                url: "/SearchAd/Filter",
                cache: false,
                type: "POST",
                dataType: "html",
                data: {
                    typeAd: typeAd,
                    city: city,
                    page: page,
                    ads: ads,
                    ArrivalDate: ArrivalDate,
                    leaveDate: leaveDate
                }
            }).done(function (partialViewResult) {
                console.log("[filter-page] onFilter - done", arguments, this);
                //console.log(partialViewResult);              
                // that.$typeAd.val('');

                $('#results').html(partialViewResult);
                document.getElementById("changeNumbersDown").value = ads;

            });
        };

        this.onFilterDown = function () {
            console.log("[filter-page] onFilter", arguments, this);

            var typeAd = that.$typeAd.val();
            var city = that.$city.val();
            var ArrivalDate = that.$arrdate.val();
            var leaveDate = that.$leavdate.val();     
            var page = 1;
            document.getElementById("currentpageUp").value = 1;
            document.getElementById("currentpageDown").value = 1; 
            var ads = that.$adsOnPageD.val();

            $.ajax({
                url: "/SearchAd/Filter",
                cache: false,
                type: "POST",
                dataType: "html",
                data: {
                    typeAd: typeAd,
                    city: city,
                    page: page,
                    ads: ads,
                    ArrivalDate: ArrivalDate,
                    leaveDate: leaveDate
                }
            }).done(function (partialViewResult) {
                console.log("[filter-page] onFilter - done", arguments, this);
                //console.log(partialViewResult);              
                // that.$typeAd.val('');

                $('#results').html(partialViewResult);
                document.getElementById("changeNumbersUp").value = ads;
               
            });
        };

        this.onDownButtons = function () {
            $("#firstDown").click(function () {
                console.log("[filter-page] $firstPage", arguments, this);
                that.onPageFilter(1);
            });

            $("#lastDown").click(function () {
                console.log("[filter-page] $lastPage", arguments, this);

                var count = parseInt(document.getElementById("count").value);
                var ads = parseInt(document.getElementById("changeNumbersDown").value);
                var lastpage = Math.ceil(count / ads);
                console.log(lastpage);
                that.onPageFilter(lastpage);
            });

            $("#nextDown").click(function () {
                console.log("[filter-page] $nextPage", arguments, this);
                var previous = parseInt(document.getElementById("currentpageDown").value);
                var current = previous + 1;

                var count = parseInt(document.getElementById("count").value);
                var ads = parseInt(document.getElementById("changeNumbersDown").value);
                var lastpage = Math.ceil(count / ads);
                if (lastpage >= current) {
                    that.onPageFilter(current);
                }
            });

            $("#previousDown").click(function () {
                console.log("[filter-page] $nextPage", arguments, this);
                var previous = parseInt(document.getElementById("currentpageDown").value);
                var current = previous - 1;
                if (current <= 0) {
                    current = 1;
                }
                that.onPageFilter(current);
            });

        };

        this.onUpButtons = function () {
            $("#firstUp").click(function () {
                console.log("[filter-page] $firstPage", arguments, this);
                that.onPageFilter(1);
            });

            $("#lastUp").click(function () {
                console.log("[filter-page] $lastPage", arguments, this);

                var count = parseInt(document.getElementById("count").value);
                var ads = parseInt(document.getElementById("changeNumbersUp").value);
                var lastpage = Math.ceil(count / ads);
                console.log(lastpage);
                that.onPageFilter(lastpage);
            });

            $("#nextUp").click(function () {
                console.log("[filter-page] $nextPage", arguments, this);
                var previous = parseInt(document.getElementById("currentpageUp").value);
                var current = previous + 1;

                var count = parseInt(document.getElementById("count").value);
                var ads = parseInt(document.getElementById("changeNumbersUp").value);
                var lastpage = Math.ceil(count / ads);
                if (lastpage >= current) {
                    that.onPageFilter(current);
                }
            });

            $("#previousUp").click(function () {
                console.log("[filter-page] $nextPage", arguments, this);
                var previous = parseInt(document.getElementById("currentpageUp").value);
                var current = previous - 1;
                if (current <= 0)
                {
                    current = 1;
                }
                that.onPageFilter(current);
            });        
    };

    };

    $(function () {
        console.log("onFilter page has loaded");
        var page = new Filter();
        page.initialize();

        page.onDownButtons();
        page.onUpButtons();

        $("#ArrivalDate").datepicker({
            currentText: "Now",
            dateFormat: "dd.mm.yy",
            constrainInput: true,
            minDate: page.today
           
        });

        $("#leaveDate").datepicker({
            currentText: "Now",
            dateFormat: "dd.mm.yy",
            constrainInput: true,
            minDate: page.today
          
        });

        $(".cbp-vm-icon").tooltip({
            items: "a[title]"
        });

        $(".showFilter").click(function () {
            $(".showFilter").hide();
            $(".hideFilter").show();
            $(".wrapp").show("slide", { direction: "up" });
        });

        $(".hideFilter").click(function () {
            $(".hideFilter").hide();
            $(".showFilter").show();
            $(".wrapp").hide("slide", { direction: "up" });
        });
            
    });

})(window.jQuery);



