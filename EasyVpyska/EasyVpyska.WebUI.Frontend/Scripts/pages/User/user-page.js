﻿(function ($) {

    var ShowAds = function () {

        var that = this;

        this.initialize = function () {
            console.log("[ShowAds-page] initialize", arguments);

            this.$showhbtn = $("#view-button");         
            this.$owner = $("#Id");

            this.$showhbtn.on("click", this.onShow);

            $('#modal-suitable-ads').dialog({
                autoOpen: false,
                modal: true,
                width: 700,
                height: 480
            });
        };

        this.onShow = function () {
            console.log("[ShowAds-page] onShow", arguments, this);
            $('#show-btn').hide();           

            var Id = that.$owner.val();
            var ads = 10;
         

            $.ajax({
                url: "/User/AllAds",
                cache: false,
                type: "POST",
                dataType: "html",
                data: {
                    Id: Id,
                    ads: ads
                }
            }).done(function (partialViewResult) {
                console.log("[ShowAds-page] onShow - done", arguments, this);
               
                $('.users-ads').html(partialViewResult);
                $('.users-ads').show();
                $('#hide-btn').show();

                that.$nexthbtn = $("#next-button");

                that.setButton();

                var count = parseInt($("#allAds").val())
                if (ads >= count) {
                    $('.next').hide();
                }
                else {
                    that.$nexthbtn.on("click", that.onShowNext);
                }       
            });
        };

        this.onShowNext = function () {
            console.log("[ShowAds-page] onShowNext", arguments, this);

            var Id = that.$owner.val();
            var ads = parseInt($("#nextAds").val()) + 10;
            var count = parseInt($("#allAds").val())

            $.ajax({
                url: "/User/AllAds",
                cache: false,
                type: "POST",
                dataType: "html",
                data: {
                    Id: Id,
                    ads: ads
                }
            }).done(function (partialViewResult) {
                console.log("[ShowAds-page] onShowNext - done", arguments, this);
                                
                document.getElementById("nextAds").value = ads;
                $('.users-ads').html(partialViewResult);
                that.$nexthbtn = $("#next-button");
                   
                that.setButton();

                that.$nexthbtn.on("click", that.onShowNext);              

                if (ads >= count)
                {
                    $('.next').hide();
                }

            });
        };

        this.onUpdateView = function () {
            console.log("[ShowAds-page] onUpdateView", arguments, this);

            var Id = that.$owner.val();
            var ads = parseInt($("#nextAds").val());
            var count = parseInt($("#allAds").val())

            console.log(ads);

            $.ajax({
                url: "/User/AllAds",
                cache: false,
                type: "POST",
                dataType: "html",
                data: {
                    Id: Id,
                    ads: ads
                }
            }).done(function (partialViewResult) {
                console.log("[ShowAds-page] onUpdateView - done", arguments, this);

                document.getElementById("nextAds").value = ads;
                $('.users-ads').html(partialViewResult);  
                that.$nexthbtn = $("#next-button");

                that.setButton();

                that.$nexthbtn.on("click", that.onShowNext);
                if (ads >= count) {
                    $('.next').hide();
                }

            });
        };

        this.onDisable = function (id,newStatus) {

            var Id = id;
            
            var status = newStatus;

            $.ajax({
                url: "/User/SetActiveAd",
                cache: false,
                type: "POST",
                dataType: "html",
                data: {
                    Id: Id,
                    status: status
                }
            }).done(function () {
                console.log("[ShowAds-page] onDisable - done", arguments, this);
                that.onUpdateView();
            });
        };

        this.onDelete = function (id) {

            var Id = id;
        
            $.ajax({
                url: "/User/DeleteAd",
                cache: false,
                type: "POST",
                dataType: "html",
                data: {
                    Id: Id                    
                }
            }).done(function () {
                console.log("[ShowAds-page] onDelete - done", arguments, this);
                that.onUpdateView();
            });
        };

        this.onSuitableAds = function (id) {

            var Id = id;
            
            $.ajax({
                url: "/User/SearchSuitableAds",
                cache: false,
                type: "POST",
                dataType: "html",
                data: {
                    Id: Id                    
                }
            }).done(function (partialViewResult) {
                console.log("[ShowAds-page] onSuitableAds - done", arguments, this);
                $('#suitableAds').html(partialViewResult);
                $("#piece").accordion();              
            });
        };

        this.setButton = function () {

            $('.user-actions  #suitable').click(function () {
                console.log("suiutable click");
                $('#modal-suitable-ads').dialog("open");
                var id = this.getAttribute("data-id");
                that.onSuitableAds(id);                       
            });

            $(".user-actions #disable").click(function () {
                console.log(this);
                var id = this.getAttribute("data-id");

                that.onDisable(id, false);
            });
            $(".user-actions #enable").click(function () {
                var id = this.getAttribute("data-id");
                that.onDisable(id, true)
            });

            $(".user-actions #delete").click(function () {
                var id = this.getAttribute("data-id");
                that.onDelete(id)
            });            
        };
    };

    $(function () {
        console.log("onFilter page has loaded");
        var page = new ShowAds();
        page.initialize();
       
        $('#hide-btn').click(function () {
            $('.users-ads').hide();
            $('#hide-btn').hide();
            $('#show-btn').show();
        });

    });

})(window.jQuery);



