﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace EasyVpyska.WebUI.Frontend.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

#region Scripts

            bundles.Add(new ScriptBundle("~/bundles/editScripts").Include(
                   "~/Scripts/pages/Edit/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/registrationScripts").Include(
                    "~/Scripts/pages/Registration/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/map").Include(
                    "~/Scripts/libs/map/*.js",
                    "~/Scripts/pages/Map/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/AdDetails").Include(                      
                   "~/Scripts/pages/AdDetails/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/CreateAd").Include(                    
                    "~/Scripts/libs/full form/*.js",
                    "~/Scripts/pages/CreateAd/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/LayoutPage").Include(                    
                    "~/Scripts/libs/JQuery/*.js",
                    "~/Scripts/libs/classie/*.js",
                    "~/Scripts/libs/inputs/*.js",
                    "~/Scripts/libs/modal window/*.js",
                    "~/Scripts/libs/slide menu/*.js",
                    "~/Scripts/pages/Layout/*.js",
                    "~/Scripts/pages/Feedback/*.js", 
                    "~/Scripts/pages/LogIn/*.js"));


            bundles.Add(new ScriptBundle("~/bundles/SearchAd").Include(             
                    "~/Scripts/pages/SearchAd/*.js",
                    "~/Scripts/libs/switch mode/*.js"));
                        
            bundles.Add(new ScriptBundle("~/bundles/homePage").Include(              
                    "~/Scripts/libs/JQuery/*.js",
                    "~/Scripts/libs/classie/*.js",
                    "~/Scripts/libs/inputs/*.js",
                    "~/Scripts/libs/modal window/*.js",
                    "~/Scripts/libs/intro effects/*.js",
                    "~/Scripts/pages/LogIn/*.js",                   
                    "~/Scripts/pages/Home/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/Profile").Include(                 
                 "~/Scripts/pages/User/*.js"));
#endregion

#region Styles

            bundles.Add(new StyleBundle("~/bundles/CreateAdPage").Include(                    
                      "~/Styles/libs/*.css",
                      "~/Styles/CreateAdPage/*.css"));

            bundles.Add(new StyleBundle("~/bundles/Layout").Include(                    
                      "~/Styles/libs/*.css",
                      "~/Styles/Layout/*.css"));

            bundles.Add(new StyleBundle("~/bundles/mapStyle").Include(                    
                      "~/Styles/libs/*.css",
                      "~/Styles/map/*.css"));

            bundles.Add(new StyleBundle("~/bundles/SearchAdPage").Include(                    
                      "~/Styles/libs/*.css",
                      "~/Styles/SearchAdPage/*.css"));

            bundles.Add(new StyleBundle("~/bundles/home").Include(                    
                      "~/Styles/libs/*.css",
                      "~/Styles/home-page.css"));

            bundles.Add(new StyleBundle("~/bundles/error").Include(                    
                      "~/Styles/libs/*.css",
                      "~/Styles/error-page.css"));

            bundles.Add(new StyleBundle("~/bundles/details").Include(                    
                      "~/Styles/libs/*.css",
                      "~/Styles/AdDetails-page.css"));

            bundles.Add(new StyleBundle("~/bundles/registration").Include(
                      "~/Styles/libs/*.css",
                      "~/Styles/registrate-page.css"));

            bundles.Add(new StyleBundle("~/bundles/edit").Include(
                        "~/Styles/libs/*.css",
                        "~/Styles/edit-page.css"));

            bundles.Add(new StyleBundle("~/bundles/user").Include(
                     "~/Styles/libs/*.css",
                     "~/Styles/profile-page.css")); 

#endregion

            BundleTable.EnableOptimizations = true; 
        }
    }
}